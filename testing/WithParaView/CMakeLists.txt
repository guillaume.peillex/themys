# This function registers a test, using paraview, which name is given in
# argument. The name is the name of the .xml file describing the test
function(register_paraview_test test_name)
  message(STATUS "Registering ParaView test ${test_name}")
  set(${test_name}_USES_DIRECT_DATA ON)
  set(${test_name}_THRESHOLD 0)
  paraview_add_client_tests(
    CLIENT ${CMAKE_BINARY_DIR}/bin/themys BASELINE_DIR
    ${CMAKE_SOURCE_DIR}/data/baseline TEST_SCRIPTS ${test_name}.xml)
  set_tests_properties(
    "pv.${test_name}"
    PROPERTIES ENVIRONMENT "PARAVIEW_DATA_ROOT=${CMAKE_SOURCE_DIR}/data" LABELS
               "Themys-GUI")
endfunction()

# This function registers a test running in client/server mode and using
# ParaView gui, which name is given in argument. The name is the name of the
# .xml file describing the test. The number of servers is given by the num_procs
# argument.
function(register_paraview_client_server_test test_name num_procs)
  message(STATUS "Registering client/server test ${test_name}")
  set(${test_name}_USES_DIRECT_DATA ON)
  set(${test_name}_THRESHOLD 0)
  paraview_add_client_server_tests(
    CLIENT
    ${CMAKE_BINARY_DIR}/bin/themys
    BASELINE_DIR
    ${CMAKE_SOURCE_DIR}/data/baseline
    TEST_SCRIPTS
    ${test_name}.xml
    NUMSERVERS
    ${num_procs})

  if(${CMAKE_VERSION} VERSION_LESS "3.22.0")
    get_property(
      test_env_properties
      TEST "pvcs.${test_name}"
      PROPERTY ENVIRONMENT)
    set_tests_properties(
      "pvcs.${test_name}"
      PROPERTIES
        ENVIRONMENT
        "${test_env_properties};PV_PLUGIN_PATH=${CMAKE_BINARY_DIR}/lib/themys;PARAVIEW_DATA_ROOT=${CMAKE_SOURCE_DIR}/data;SMTESTDRIVER_MPI_PREFLAGS=--use-hwthread-cpus"
    )
  else()
    set_tests_properties(
      "pvcs.${test_name}"
      PROPERTIES
        ENVIRONMENT_MODIFICATION
        "PV_PLUGIN_PATH=set:${CMAKE_BINARY_DIR}/lib/themys;PARAVIEW_DATA_ROOT=set:${CMAKE_SOURCE_DIR}/data;SMTESTDRIVER_MPI_PREFLAGS=set:--use-hwthread-cpus"
    )
  endif()

  # Adds label to the test so that it can be excluded from the CI with ctest -LE
  # require-mpi
  set_tests_properties("pvcs.${test_name}" PROPERTIES LABELS "Themys-GUI")
endfunction()

register_paraview_test(OpenFile)
register_paraview_client_server_test(OpenFile 4)

register_paraview_test(InterfaceModes)
register_paraview_client_server_test(InterfaceModes 4)
