#ifndef pqThemysAssistantFileDialog_h
#define pqThemysAssistantFileDialog_h

#include <QScopedPointer>

#include <pqFileDialog.h>

class pqServer;
/**
 */
class pqThemysAssistantFileDialog : public pqFileDialog
{
  typedef pqFileDialog Superclass;
  Q_OBJECT
public:
  /**
   */
  pqThemysAssistantFileDialog(pqServer* server, QWidget* parent,
                              const QString& title = QString(),
                              const QString& directory = QString(),
                              const QString& filter = QString(),
                              bool groupFiles = true);
  ~pqThemysAssistantFileDialog() override;

public Q_SLOTS:
  void toggleAssistant(bool toggle);
  void acceptBase(const QString& base);

private:
  pqThemysAssistantFileDialog(const pqThemysAssistantFileDialog&);
  pqThemysAssistantFileDialog& operator=(const pqThemysAssistantFileDialog&);

  struct pqInternals;
  QScopedPointer<pqInternals> Internals;
};

#endif // !pqThemysAssistantFileDialog_h
