#ifndef pqThemysSettingsReaction_h
#define pqThemysSettingsReaction_h

#include <QPointer>

#include "pqReaction.h"

class pqSettingsDialog;

/**
 * @ingroup Reactions
 * pqThemysSettingsReaction is a reaction to popup the application
 * settings dialog. It creates pqSettingsDialog when required.
 */
class pqThemysSettingsReaction : public pqReaction
{
  Q_OBJECT
  typedef pqReaction Superclass;

public:
  /**
   * Constructor. Parent cannot be nullptr.
   */
  pqThemysSettingsReaction(QAction* parent);
  ~pqThemysSettingsReaction() override;

  /**
   * Show the application settings dialog.
   */
  static void showApplicationSettingsDialog(const QString& tabName = "");

protected:
  /**
   * Called when the action is triggered.
   */
  void onTriggered() override
  {
    pqThemysSettingsReaction::showApplicationSettingsDialog();
  }

private:
  Q_DISABLE_COPY(pqThemysSettingsReaction)

  static QPointer<pqSettingsDialog> Dialog;
};

#endif
