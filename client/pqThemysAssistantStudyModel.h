#ifndef pqThemysAssistantStudyModel_h
#define pqThemysAssistantStudyModel_h

#include <QAbstractItemModel>

#include "pqThemysAssistantBaseManager.h"

class QSortFilterProxyModel;

/**
 * Model for representing the available studies in Themys Assistant.
 */
class pqThemysAssistantStudyModel : public QAbstractItemModel
{
  typedef QAbstractItemModel Superclass;
  Q_OBJECT;

public:
  pqThemysAssistantStudyModel(pqServer* server,
                              pqThemysAssistantBaseManager* manager,
                              QObject* parent = nullptr);

  virtual ~pqThemysAssistantStudyModel();

  bool setData(const QModelIndex& idx, const QVariant& value,
               int role) override;
  QVariant data(const QModelIndex& idx, int role) const override;

  /**
   * Defines the available columns for a tree view.
   */
  QVariant headerData(int section, Qt::Orientation, int role) const override;

  //@{
  /**
   * Pure virtual function to override from Superclass
   */
  int columnCount(const QModelIndex&) const override;
  QModelIndex index(int row, int column, const QModelIndex&) const override;
  QModelIndex parent(const QModelIndex&) const override;
  int rowCount(const QModelIndex&) const override;
  bool hasChildren(const QModelIndex& idx) const override;
  Qt::ItemFlags flags(const QModelIndex& idx) const override;
  //@}

  /**
   * Proxy model used for things such as sorting, filtering, etc.
   */
  QSortFilterProxyModel* proxyModel() const { return this->ProxyModel; }

  //@{
  /**
   * Functions for adding/removing studies in the model.
   * When adding the study date will be set to the current date and it will
   * return the row index it has been added at.
   */
  int addStudy(const QString& name = "Study");
  void removeStudy(int row);
  void clearStudies();
  //@}

  //@{
  /**
   * API for editing current studies. One can mark a study so it is easier to
   * retrieve it later on (usually one will mark the currently displayed study).
   *
   * XXX: maybe the active study API should be redesigned and live in the base
   * model instead, as a QPersistentModelIndex.
   */
  void markActiveStudy(int idx) { this->ActiveStudy = idx; }
  int activeStudy() const { return this->ActiveStudy; }
  //@}

  //@{
  /**
   * Add/Remove/Modify bases in the given study.
   */
  void renameBaseInStudy(int studyIdx, int baseIdx, const QString& newName);
  void addBaseToStudy(int studyIdx, const assistant::Base& base);
  void removeBaseFromStudy(int studyIdx, const assistant::Base& base);
  //@}

Q_SIGNALS:
  void activeStudyRenamed(const QString& newName) const;

private:
  /**
   * Build a Study object from a string representing a JSON object. Return
   * true if JSON could be correclty processed, false otherwise.
   */
  bool ParseStudy(const nlohmann::json& studyJson, assistant::Study& res);

  /**
   * Build a string representing a JSON object for the given study.
   */
  nlohmann::json SerializeStudy(const assistant::Study& study);

  QSortFilterProxyModel* const ProxyModel;
  // Underlying data exposed by this class.
  QVector<assistant::Study> Studies;
  pqThemysAssistantBaseManager* BaseManager;
  int ActiveStudy;

  static constexpr const char* STUDY_SETTING_KEY = ".Studies";
  QString ServerURI;
};

#endif // pqThemysAssistantStudyModel_h
