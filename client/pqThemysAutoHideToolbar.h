#include <QToolBar>

class QActionEvent;

/**
 * pqThemysAutoHideToolbar is a subclass of QToolBar thats implement
 * an auto-hide feature:
 *  - hidden on ActionDelete event leading to an empty toolbar,
 *  - shown on ActionAdded event on an empty toolbar.
 */
class pqThemysAutoHideToolbar : public QToolBar
{
  Q_OBJECT;
  using Superclass = QToolBar;

public:
  pqThemysAutoHideToolbar(const QString& title, QWidget* parent = nullptr);
  ~pqThemysAutoHideToolbar() override = default;

protected slots:

  /**
   * Slot called when an action is modified/added/deleted.
   */
  void actionEvent(QActionEvent* event) override;

private:
  Q_DISABLE_COPY(pqThemysAutoHideToolbar);
};
