#include "pqThemysAssistantFileDialog.h"

#include <QGridLayout>
#include <QLayout>
#include <QLayoutItem>
#include <QTabWidget>
#include <QToolButton>
#include <QVBoxLayout>
#include <iostream>
#include <string>

#include <pqFileDialog.h>

#include "pqThemysAssistantMainWidget.h"
//------------------------------------------------------------------------------
struct pqThemysAssistantFileDialog::pqInternals {
  QWidget* FileDialogWidget;
  pqThemysAssistantMainWidget* AssistantWidget;
  QToolButton* AssistantButtonFileDialog;
};

//-----------------------------------------------------------------------------
pqThemysAssistantFileDialog::pqThemysAssistantFileDialog(
    pqServer* server, QWidget* parent, const QString& title,
    const QString& startDirectory, const QString& nameFilter, bool groupFiles)
    : Superclass(server, parent, title, startDirectory, nameFilter, groupFiles),
      Internals(new pqThemysAssistantFileDialog::pqInternals())
{
  QLayout* crtLayout = this->layout();
  if (crtLayout == nullptr)
  {
    qWarning("QLayout* is nullptr [step 1].");
    return;
  }
  QVBoxLayout* newmainLayout = qobject_cast<QVBoxLayout*>(crtLayout);
  if (newmainLayout == nullptr)
  {
    qWarning("QLayout* is not a QVBoxLayout* [step 2].");
    return;
  }
  QLayoutItem* crtLayoutItem = newmainLayout->itemAt(0);
  if (crtLayoutItem == nullptr)
  {
    qWarning("QLayoutItem* is nullptr [step 3].");
    return;
  }
  QWidget* crtWidget = crtLayoutItem->widget();
  if (crtWidget == nullptr)
  {
    qWarning("QWidget* is nullptr [step 4].");
    return;
  }
  QTabWidget* newTabWidget = qobject_cast<QTabWidget*>(crtWidget);
  if (newTabWidget == nullptr)
  {
    qWarning("QWidget* is not a QTabWidget* [step 5].");
    return;
  }
  QWidget* mainwidget = nullptr;
  if (newTabWidget->count() >= 1)
  {
    // Set the first tab by default
    mainwidget = newTabWidget->widget(0);
    for (unsigned int iTab = 0; iTab < newTabWidget->count(); ++iTab)
    {
      QWidget* tmp = newTabWidget->widget(iTab);
      if (tmp == nullptr)
      {
        continue;
      }
      if (newTabWidget->tabText(iTab).compare("Remote File System") == 0)
      {
        // If the name tab is Remote File System and tab is non nullptr
        // then set mainwidget and break the for
        mainwidget = tmp;
        break;
      } else if (mainwidget == nullptr)
      {
        // If the first tab by default is nullptr then set a tab non nullptr
        mainwidget = tmp;
      }
    }
  }
  if (mainwidget == nullptr)
  {
    // mainwidget is nullptr if no tab is non nullptr
    qWarning("QWidget* is nullptr [step 6].");
    return;
  }
  crtLayout = mainwidget->layout();
  if (crtLayout == nullptr)
  {
    qWarning("QLayout* is nullptr [step 7].");
    return;
  }
  QGridLayout* mainLayout = qobject_cast<QGridLayout*>(crtLayout);
  if (mainLayout == nullptr)
  {
    qWarning("QLayout* is not a QGridLayout* [step 8].");
    return;
  }
  crtLayoutItem = mainLayout->itemAtPosition(0, 0);
  if (crtLayoutItem == nullptr)
  {
    qWarning("QLayoutItem* is nullptr [step 9].");
    return;
  }
  this->Internals->FileDialogWidget = crtLayoutItem->widget();
  if (this->Internals->FileDialogWidget == nullptr)
  {
    qWarning("QWidget* is nullptr [step 10].");
    return;
  }
  crtLayout = this->Internals->FileDialogWidget->layout();
  if (crtLayout == nullptr)
  {
    qWarning("QLayout* is nullptr [step 11].");
    return;
  }
  QGridLayout* gridLayout = qobject_cast<QGridLayout*>(crtLayout);
  if (gridLayout == nullptr)
  {
    qWarning("QLayout* is not QGridLayout* [step 12].");
    return;
  }
  crtLayoutItem = gridLayout->itemAtPosition(0, 0);
  if (crtLayoutItem == nullptr)
  {
    qWarning("QLayoutItem* is nullptr [step 13].");
    return;
  }
  crtLayout = crtLayoutItem->layout();
  if (crtLayout == nullptr)
  {
    qWarning("QLayout* is nullptr [step 14].");
    return;
  }
  QHBoxLayout* hboxLayout = qobject_cast<QHBoxLayout*>(crtLayout);
  if (hboxLayout == nullptr)
  {
    qWarning("QLayout* is not QHBoxLayout* [step 15].");
    return;
  }

  this->Internals->AssistantButtonFileDialog = new QToolButton(this);
  if (this->Internals->AssistantButtonFileDialog == nullptr)
  {
    qWarning("QToolButton* is nullptr, error allocate memory [step 16].");
    return;
  }
  this->Internals->AssistantButtonFileDialog->setText("Assistant");
  this->Internals->AssistantButtonFileDialog->setCheckable(true);
  hboxLayout->insertWidget(0, this->Internals->AssistantButtonFileDialog);
  this->Internals->AssistantWidget =
      new pqThemysAssistantMainWidget(server, this);
  if (this->Internals->AssistantWidget == nullptr)
  {
    delete[] this->Internals->AssistantButtonFileDialog;
    qWarning("pqThemysAssistantMainWidget* is nullptr, error allocate memory "
             "[step 17].");
    return;
  }
  mainLayout->addWidget(this->Internals->AssistantWidget, 1, 0);
  this->Internals->AssistantWidget->setVisible(false);

  QObject::connect(this->Internals->AssistantButtonFileDialog,
                   &QToolButton::clicked, this,
                   &pqThemysAssistantFileDialog::toggleAssistant);
  QObject::connect(this->Internals->AssistantWidget,
                   &pqThemysAssistantMainWidget::toggleAssistantClicked, this,
                   &pqThemysAssistantFileDialog::toggleAssistant);
  QObject::connect(this->Internals->AssistantWidget,
                   &pqThemysAssistantMainWidget::baseAccepted, this,
                   &pqThemysAssistantFileDialog::acceptBase);
  QObject::connect(this->Internals->AssistantWidget,
                   &pqThemysAssistantMainWidget::cancelClicked, this,
                   &pqThemysAssistantFileDialog::reject);
}

//-----------------------------------------------------------------------------
pqThemysAssistantFileDialog::~pqThemysAssistantFileDialog() = default;

//-----------------------------------------------------------------------------
void pqThemysAssistantFileDialog::toggleAssistant(bool toggle)
{
  this->Internals->FileDialogWidget->setVisible(!toggle);
  this->Internals->AssistantButtonFileDialog->setChecked(toggle);
  this->Internals->AssistantWidget->setVisible(toggle);
  this->Internals->AssistantWidget->setToggleAssistantStatus(toggle);
}

//-----------------------------------------------------------------------------
void pqThemysAssistantFileDialog::acceptBase(const QString& base)
{
  // Select a file and accept the window
  this->selectFile(base);
}
