#ifndef pqThemysAssistantStorageModel_h
#define pqThemysAssistantStorageModel_h

#include <QAbstractItemModel>
#include <QString>
#include <QVector>

class pqServer;

/**
 * Model for representing the available storages in Themys Assistant storage
 * list.
 */
class pqThemysAssistantStorageModel : public QAbstractItemModel
{
  typedef QAbstractItemModel Superclass;
  Q_OBJECT
public:
  pqThemysAssistantStorageModel(pqServer* server, QObject* parent);
  ~pqThemysAssistantStorageModel() override = default;

  int columnCount(const QModelIndex&) const override;
  QModelIndex index(int row, int column, const QModelIndex&) const override;
  QModelIndex parent(const QModelIndex&) const override;
  int rowCount(const QModelIndex& idx) const override;

  /**
   * Provides storageName for display/edit role
   * Provides evaluated command for user/tooltip role
   */
  QVariant data(const QModelIndex& idx, int role) const override;

  /**
   * Get the path of a given storageName by evaluating the command
   * with the already set command argument
   */
  QString getStoragePath(const QString& storageName) const;

  ///@{
  /**
   * Set/Get the command argument
   * default is empty
   */
  void setCommandArgument(const QString& argument);
  QString commandArgument();
  ///@}

protected:
  /**
   * Evaluate the given command with the given argument and return its standard
   * output if no error occured.
   */
  QString evaluatePathFromCommand(const QString& command,
                                  const QString& argument) const;

private:
  void LoadCommandsFromSettings();

  QMap<QString, QString> StorageCommands;
  QString ServerURI;
  pqServer* Server;
  QString CommandArgument;
};

#endif // pqThemysAssistantStorageModel_h
