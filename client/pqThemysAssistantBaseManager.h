#ifndef pqThemysAssistantBaseManager_h
#define pqThemysAssistantBaseManager_h

#include <QDateTime>
#include <QList>
#include <QObject>
#include <QString>
#include <QVector>

#include <vtkSmartPointer.h>
#include <vtk_nlohmannjson.h>
#include VTK_NLOHMANN_JSON(json.hpp)

#include <iostream>

class pqServer;
class pqThemysAssistantStorageModel;
class vtkSMProxy;
class vtkPVFileInformation;
class vtkPVFileInformationHelper;

namespace assistant {
/**
 * Structure representing a single base.
 */
struct Base {
  QString Name;
  QString StorageName;
  QString User;
  QString Case;
  QString Identifier;
  QDateTime Date;
  QStringList FileList;

  bool operator==(const Base& rx) const
  {
    return this->StorageName == rx.StorageName && this->User == rx.User &&
           this->Case == rx.Case && this->Identifier == rx.Identifier;
  }
};

/**
 * Structure representing a collection of base, named Study.
 */
struct Study {
  QString Name;
  QDateTime Date;
  QVector<Base> Bases;
};
} // namespace assistant

/**
 * Manager for creating bases and save/get the recently opened bases.
 * Also provides an interface for having some server side information
 * of the filesystem.
 */
class pqThemysAssistantBaseManager : public QObject
{
  typedef QObject Superclass;
  Q_OBJECT;

public:
  pqThemysAssistantBaseManager(pqServer* server,
                               pqThemysAssistantStorageModel* storageModel,
                               QObject* parent = nullptr);
  virtual ~pqThemysAssistantBaseManager();

  struct FileInfo {
    QString Name;
    QString FullPath;
    QDateTime Date;
  };
  typedef QVector<FileInfo> IndexList;

  /**
   * Return a correctly initialized vector of bases given a storage name, user,
   * case and vector of regexs. It is more efficient when creating multiple
   * bases from a single path. Populate the bases.
   */
  QVector<assistant::Base> CreateBases(const QString& storageName,
                                       const QString& user,
                                       const QString& curCase,
                                       const QVector<QString>& regexes) const;

  /**
   * Given a base identifier return a list of file belonging to this base.
   * Can take some time as it browse the filesystem.
   */
  QStringList GetBaseFileList(const QString& identifier) const;

  /**
   * Given a base identifier return the first file belonging to this base.
   * If no file was found for this base then returns an empty string.
   */
  QString GetBaseFirstFile(const QString& identifier) const;

  /**
   * Get the absolute directory path for a given storage name, user and case.
   * Return an empty string if cannot construct the path.
   */
  QString GetBasePath(const QString& storageName, const QString& user,
                      const QString& curCase) const;

  /**
   * Return a list of <DirName, FullPath> of all non hidden dirs in 'path'.
   * List will be empty if 'path' does not point to a directory.
   */
  IndexList GetDirectoryList(const QString& path) const;

  /**
   * Given a base, return a JSON object representing it.
   */
  nlohmann::json SerializeBase(const assistant::Base& base) const;

  /**
   * Given a JSON object, try to construct a base from it. Return true
   * if base construction is correct, false otherwise
   */
  bool ParseBase(const nlohmann::json& base, assistant::Base& result) const;

  /**
   * Add the given base to the list of the recent bases. Mark its date with the
   * current date.
   */
  void AddToRecentBase(assistant::Base base);

  const QList<assistant::Base>& recentBases() const
  {
    return this->RecentBases;
  }
  const QVector<QString>& basesRegex() const { return this->BasesRegex; }

private:
  /**
   * Recursive function call for CreateBases()
   */
  void CreateBasesRecursive(QString path, const QVector<QString>& regexes,
                            int depth, const int maxDepth,
                            QVector<assistant::Base>& result) const;

  /**
   * Get the information instance for a specific folder/file.
   * Does not support relative path and does not group files together.
   */
  vtkSmartPointer<vtkPVFileInformation> GetInformations(const QString& path,
                                                        bool details) const;

  pqThemysAssistantStorageModel* StorageModel;
  vtkSmartPointer<vtkPVFileInformationHelper> FileInformationHelper;
  vtkSmartPointer<vtkSMProxy> FileInformationHelperProxy;

  /**
   * Model for storing the recently opened bases.
   */
  static constexpr int HISTORY_SIZE = 10;
  static constexpr const char* RECENT_BASES_KEY = ".RecentBases";
  QString ServerURI;

  QList<assistant::Base> RecentBases;
  QVector<QString> BasesRegex;
};

Q_DECLARE_METATYPE(assistant::Base);

#endif // pqThemysAssistantBaseManager_h
