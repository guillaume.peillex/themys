#include "pqThemysAssistantBaseModel.h"

#include <QDateTime>
#include <QFileIconProvider>
#include <QSortFilterProxyModel>

// ----------------------------------------------------------------------------
pqThemysAssistantBaseModel::pqThemysAssistantBaseModel(QObject* parent)
    : Superclass(parent), ProxyModel(new QSortFilterProxyModel(this))
{
  ProxyModel->setSourceModel(this);
}

// ----------------------------------------------------------------------------
int pqThemysAssistantBaseModel::columnCount(const QModelIndex&) const
{
  // Name, Storage, User, Case, Date
  return 5;
}

// ----------------------------------------------------------------------------
bool pqThemysAssistantBaseModel::setData(const QModelIndex& idx,
                                         const QVariant& value, int role)
{
  Q_UNUSED(role);

  int row = idx.row();
  if (row >= this->Bases.size() || idx.column() != 0)
  {
    return false;
  }

  QString newName = value.toString();
  this->Bases[row].Name = newName;
  if (this->CurrentState == BaseDisplayState::STUDY)
  {
    Q_EMIT this->studyBaseNameChanged(row, newName);
  }

  return true;
}

// ----------------------------------------------------------------------------
QVariant pqThemysAssistantBaseModel::data(const QModelIndex& idx,
                                          int role) const
{
  if (idx.row() >= this->Bases.size())
  {
    return QVariant();
  }

  const assistant::Base& base = this->Bases[idx.row()];
  switch (role)
  {
  case Qt::DisplayRole:
  case Qt::EditRole: {
    switch (idx.column())
    {
    case 0:
      return base.Name;
    case 1:
      return base.StorageName;
    case 2:
      return base.User;
    case 3:
      return base.Case;
    case 4:
      return base.Date;
    default:
      return QVariant();
    }
  }
  case Qt::UserRole:
    return base.FileList;
  case Qt::ToolTipRole:
    return base.Identifier;
  default:
    return QVariant();
  }
}

// ----------------------------------------------------------------------------
QModelIndex pqThemysAssistantBaseModel::index(int row, int column,
                                              const QModelIndex&) const
{
  return this->createIndex(row, column);
}

// ----------------------------------------------------------------------------
QModelIndex pqThemysAssistantBaseModel::parent(const QModelIndex&) const
{
  return QModelIndex();
}

// ----------------------------------------------------------------------------
int pqThemysAssistantBaseModel::rowCount(const QModelIndex&) const
{
  return this->Bases.size();
}

// ----------------------------------------------------------------------------
bool pqThemysAssistantBaseModel::hasChildren(const QModelIndex& idx) const
{
  if (!idx.isValid())
  {
    return true;
  }

  // Bases will never display childrens.
  // XXX It could be possible to display all files that belongs to a base
  // though.
  return false;
}

// ----------------------------------------------------------------------------
QVariant pqThemysAssistantBaseModel::headerData(int section, Qt::Orientation,
                                                int role) const
{
  if (role == Qt::DisplayRole)
  {
    switch (section)
    {
    case 0:
      return tr("Base name");
    case 1:
      return tr("Storage");
    case 2:
      return tr("User");
    case 3:
      return tr("Case");
    case 4:
      return tr("Date");
    default:
      return QVariant();
    }
  }

  return QVariant();
}

// ----------------------------------------------------------------------------
Qt::ItemFlags pqThemysAssistantBaseModel::flags(const QModelIndex& idx) const
{
  Qt::ItemFlags flags = Qt::ItemIsSelectable | Qt::ItemIsEnabled;
  if (this->CurrentState != BaseDisplayState::FILTERED_BASE &&
      idx.column() == 0)
  {
    flags |= Qt::ItemIsEditable;
  }
  return flags;
}

// ----------------------------------------------------------------------------
void pqThemysAssistantBaseModel::setBases(const QVector<assistant::Base>& bases,
                                          BaseDisplayState state)
{
  this->beginResetModel();
  this->Bases = std::move(bases);
  this->CurrentState = state;
  this->endResetModel();
}
