#include "pqThemysSettingsReaction.h"

#include <pqCoreUtilities.h>
#include <pqSettingsDialog.h>

QPointer<pqSettingsDialog> pqThemysSettingsReaction::Dialog;

//-----------------------------------------------------------------------------
pqThemysSettingsReaction::pqThemysSettingsReaction(QAction* parentObject)
    : Superclass(parentObject)
{
}

//-----------------------------------------------------------------------------
pqThemysSettingsReaction::~pqThemysSettingsReaction()
{
  delete pqThemysSettingsReaction::Dialog;
}

//-----------------------------------------------------------------------------
void pqThemysSettingsReaction::showApplicationSettingsDialog(
    const QString& tabName)
{
  if (!pqThemysSettingsReaction::Dialog)
  {
    pqThemysSettingsReaction::Dialog =
        new pqSettingsDialog(pqCoreUtilities::mainWidget(), Qt::WindowFlags(),
                             QStringList("Themys"));
    pqThemysSettingsReaction::Dialog->setObjectName("ApplicationSettings");
    pqThemysSettingsReaction::Dialog->setAttribute(Qt::WA_DeleteOnClose, true);
  }
  pqThemysSettingsReaction::Dialog->show();
  pqThemysSettingsReaction::Dialog->raise();
  pqThemysSettingsReaction::Dialog->showTab(tabName);
}
