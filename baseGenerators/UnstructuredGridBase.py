from BaseBuilder import *

import math
import os
import sys
import vtk

absCellIndex = 0


# -----------------------------------------------------------------------------
# Inpired from https://kitware.github.io/vtk-examples/site/Python/StructuredGrid/SGrid/
def BuildDomain(timeIdx, matIdx, domainIdx, isTwin):
    lenDomains = len(domains)
    suffixTwin = "_Twin" if isTwin else ""

    translation = [
        [[0, 0, 0], [0, 1.0, 0], [0, 2.0, 0]],
        [[2.0, 1.0, 0], [2.0, 2.0, 0], [2.0, 0, 0]],
    ]

    # Create the structured grid.
    ugrid = vtk.vtkUnstructuredGrid()

    x = [
        [0, 0, 0],
        [1, 0, 0],
        [2, 0, 0],
        [0, 1, 0],
        [1, 1, 0],
        [2, 1, 0],
        [0, 0, 1],
        [1, 0, 1],
        [2, 0, 1],
        [0, 1, 1],
        [1, 1, 1],
        [2, 1, 1],
        [0, 1, 2],
        [1, 1, 2],
        [2, 1, 2],
        [0, 1, 3],
        [1, 1, 3],
        [2, 1, 3],
        [0, 1, 4],
        [1, 1, 4],
        [2, 1, 4],
        [0, 1, 5],
        [1, 1, 5],
        [2, 1, 5],
        [0, 1, 6],
        [1, 1, 6],
        [2, 1, 6],
    ]
    # Here we have kept consistency with the Cxx example of the same name.
    # This means we will use slicing in ugrid.InsertNextCell to ensure that the correct
    #  number of points are used.
    pts = [
        [0, 1, 4, 3, 6, 7, 10, 9],
        [1, 2, 5, 4, 7, 8, 11, 10],
        [6, 10, 9, 12, 0, 0, 0, 0],
        [8, 11, 10, 14, 0, 0, 0, 0],
        [16, 17, 14, 13, 12, 15, 0, 0],
        [18, 15, 19, 16, 20, 17, 0, 0],
        [22, 23, 20, 19, 0, 0, 0, 0],
        [21, 22, 18, 0, 0, 0, 0, 0],
        [22, 19, 18, 0, 0, 0, 0, 0],
        [23, 26, 0, 0, 0, 0, 0, 0],
        [21, 24, 0, 0, 0, 0, 0, 0],
        [25, 0, 0, 0, 0, 0, 0, 0],
    ]

    orig = [1.0, 0.5, 3.0]
    vecData = [[i - j for i, j in zip(orig, a)] for a in x]
    vecLength = [math.sqrt(sum((a * a for a in scalVec))) for scalVec in vecData]

    vectorArray = vtk.vtkDoubleArray()
    vectorArray.SetNumberOfComponents(3)
    vectorArray.SetName("VectorData" + suffixTwin)
    scalarArray = vtk.vtkDoubleArray()
    scalarArray.SetNumberOfComponents(1)
    scalarArray.SetName("ScalarData" + suffixTwin)
    stringArray = vtk.vtkStringArray()
    stringArray.SetName("StringData" + suffixTwin)

    points = vtk.vtkPoints()
    for i in range(0, len(x)):
        points.InsertPoint(i, x[i])
        vectorArray.InsertNextTuple3(vecData[i][0], vecData[i][1], vecData[i][2])
        scalarArray.InsertNextValue(vecLength[i] * (timeIdx + 1))
        stringArray.InsertNextValue("Point " + str(i))

    numVerticesArray = vtk.vtkIntArray()
    numVerticesArray.SetNumberOfValues(12)
    numVerticesArray.SetName("NumberOfVertices" + suffixTwin)
    cellIndexArray = vtk.vtkIntArray()
    cellIndexArray.SetNumberOfValues(12)
    cellIndexArray.SetName("CellAbsoluteIndex" + suffixTwin)
    cellNbStrArray = vtk.vtkStringArray()
    cellNbStrArray.SetNumberOfValues(12)
    cellNbStrArray.SetName("CellNumber" + suffixTwin)

    global absCellIndex
    numVerticesArrayData = [8, 8, 4, 4, 6, 6, 4, 3, 3, 2, 2, 1]
    for i, val in enumerate(numVerticesArrayData):
        numVerticesArray.SetValue(i, val)
        cellIndexArray.SetValue(i, absCellIndex)
        cellNbStrArray.SetValue(i, "Cell " + str(i))
        absCellIndex += 1

    ugrid = vtk.vtkUnstructuredGrid()
    ugrid.Allocate(100)
    ugrid.InsertNextCell(vtk.VTK_HEXAHEDRON, 8, pts[0])
    ugrid.InsertNextCell(vtk.VTK_HEXAHEDRON, 8, pts[1])
    ugrid.InsertNextCell(vtk.VTK_TETRA, 4, pts[2][:4])
    ugrid.InsertNextCell(vtk.VTK_TETRA, 4, pts[3][:4])
    ugrid.InsertNextCell(vtk.VTK_POLYGON, 6, pts[4][:6])
    ugrid.InsertNextCell(vtk.VTK_TRIANGLE_STRIP, 6, pts[5][:6])
    ugrid.InsertNextCell(vtk.VTK_QUAD, 4, pts[6][:4])
    ugrid.InsertNextCell(vtk.VTK_TRIANGLE, 3, pts[7][:3])
    ugrid.InsertNextCell(vtk.VTK_TRIANGLE, 3, pts[8][:3])
    ugrid.InsertNextCell(vtk.VTK_LINE, 2, pts[9][:2])
    ugrid.InsertNextCell(vtk.VTK_LINE, 2, pts[10][:2])
    ugrid.InsertNextCell(vtk.VTK_VERTEX, 1, pts[11][:1])

    ugrid.SetPoints(points)
    ugrid.GetPointData().AddArray(vectorArray)
    ugrid.GetPointData().AddArray(scalarArray)
    ugrid.GetPointData().AddArray(stringArray)
    ugrid.GetCellData().AddArray(cellIndexArray)
    ugrid.GetCellData().AddArray(numVerticesArray)
    ugrid.GetCellData().AddArray(cellNbStrArray)

    transform = vtk.vtkTransform()
    transform.Translate(translation[matIdx][domainIdx])
    transformFilter = vtk.vtkTransformFilter()
    transformFilter.SetInputData(ugrid)
    transformFilter.SetTransform(transform)
    transformFilter.Update()

    ugrid.SetPoints(transformFilter.GetOutput().GetPoints())

    return ugrid


# -----------------------------------------------------------------------------
if __name__ == "__main__":
    args = sys.argv
    if len(args) < 2:
        print("Path to output directory should be specified")
        sys.exit()

    path = args[1]
    if isinstance(path, str) is False or os.path.isdir(path) is False:
        print("Invalid directory:", path)
        sys.exit()

    Build(path, "USGBase", "USG Base Example", BuildDomain, False)
    # Reset absolute cell indices
    absCellIndex = 0
    Build(path, "USGBase_Twin", "USG Base Example (Twin)", BuildDomain, True)
