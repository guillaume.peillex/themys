from BaseBuilder import *

import math
import os
import sys
import vtk


# -----------------------------------------------------------------------------
# Inpired from https://kitware.github.io/vtk-examples/site/Python/StructuredGrid/SGrid/
def BuildDomain(timeIdx, matIdx, domainIdx, isTwin):
    lenDomains = len(domains)
    suffixTwin = "_Twin" if isTwin else ""
    rMin = domainIdx / lenDomains
    rMax = (domainIdx + 1) / lenDomains
    dims = [13, 11, 11]

    # Create the structured grid.
    sgrid = vtk.vtkStructuredGrid()
    sgrid.SetDimensions(dims)

    if matIdx > 0:
        rMax = -rMax
        rMin = -rMin

    nPoints = dims[0] * dims[1] * dims[2]

    # We also create the points and vectors. The points
    # form a hemi-cylinder of data.
    vectors = vtk.vtkDoubleArray()
    vectors.SetNumberOfComponents(3)
    vectors.SetNumberOfTuples(nPoints)
    vectors.SetName("VectorData" + suffixTwin)
    scalars = vtk.vtkDoubleArray()
    scalars.SetNumberOfComponents(1)
    scalars.SetNumberOfTuples(nPoints)
    scalars.SetName("ScalarData" + suffixTwin)
    strings = vtk.vtkStringArray()
    strings.SetNumberOfTuples(nPoints)
    strings.SetName("StringData" + suffixTwin)
    points = vtk.vtkPoints()
    points.Allocate(nPoints)

    deltaZ = 2.0 / (dims[2] - 1)
    deltaRad = (rMax - rMin) / (dims[1] - 1)
    x = [0.0] * 3
    v = [0.0] * 3
    for k in range(0, dims[2]):
        x[2] = -1.0 + k * deltaZ
        kOffset = k * dims[0] * dims[1]
        for j in range(0, dims[1]):
            radius = rMin + j * deltaRad
            jOffset = j * dims[0]
            for i in range(0, dims[0]):
                theta = i * vtk.vtkMath.RadiansFromDegrees(15.0)
                x[0] = radius * math.sin(theta)
                x[1] = radius * math.cos(theta)
                # change tangent rotation if timeIdx change
                v[0] = -x[1] if (timeIdx % 2) else x[1]
                v[1] = x[0] if (timeIdx % 2) else -x[0]
                offset = i + jOffset + kOffset
                points.InsertPoint(offset, x)
                vectors.InsertTuple(offset, v)
                # Set to distance from origin * timeindex
                scalars.SetValue(
                    offset, float(math.sqrt(sum((i**2 for i in x))) * (timeIdx + 1))
                )
                strings.SetValue(offset, "Point " + str(offset))

    sgrid.SetPoints(points)
    sgrid.GetPointData().AddArray(scalars)
    sgrid.GetPointData().AddArray(vectors)
    sgrid.GetPointData().AddArray(strings)

    # Cell data arrays

    qualityFilter = vtk.vtkMeshQuality()
    qualityFilter.SetInputData(sgrid)
    qualityFilter.SaveCellQualityOn()
    qualityFilter.SetHexQualityMeasureToVolume()
    qualityFilter.Update()
    volumeArray = qualityFilter.GetOutput().GetCellData().GetArray("Quality")
    # This function BuildDomain generate a fraction of a cylinder of r = 0.5 and h = 1.0
    # Total volume of this cylinder is .78539816, but we compute the cell volume relative to the material
    totalVolume = 0.78539816 / len(materials)
    relativeVol = volumeArray.NewInstance()
    relativeVol.SetNumberOfValues(volumeArray.GetNumberOfValues())
    relativeVol.SetName("RelativeCellVolume" + suffixTwin)
    for i in range(0, volumeArray.GetNumberOfValues()):
        relativeVol.SetValue(i, volumeArray.GetValue(i) / totalVolume)

    qualityFilter = vtk.vtkMeshQuality()
    qualityFilter.SetInputData(sgrid)
    qualityFilter.SaveCellQualityOn()
    qualityFilter.SetHexQualityMeasureToSkew()
    qualityFilter.Update()
    skewArray = qualityFilter.GetOutput().GetCellData().GetArray("Quality")
    skewArray.SetName("SkewQuality" + suffixTwin)

    cellNbStrArray = vtk.vtkStringArray()
    cellNbStrArray.SetNumberOfTuples(sgrid.GetNumberOfCells())
    cellNbStrArray.SetName("CellNumber" + suffixTwin)
    for i in range(0, sgrid.GetNumberOfCells()):
        cellNbStrArray.SetValue(i, "Cell " + str(i))

    sgrid.GetCellData().AddArray(relativeVol)
    sgrid.GetCellData().AddArray(skewArray)
    sgrid.GetCellData().AddArray(cellNbStrArray)

    return sgrid


# -----------------------------------------------------------------------------
if __name__ == "__main__":
    args = sys.argv
    if len(args) < 2:
        print("Path to output directory should be specified")
        sys.exit()

    path = args[1]
    if isinstance(path, str) is False or os.path.isdir(path) is False:
        print("Invalid directory:", path)
        sys.exit()

    Build(path, "SG3DBase", "SG 3D Base Example", BuildDomain, False)
    Build(path, "SG3DBase_Twin", "SG 3D Base Example (Twin)", BuildDomain, True)
