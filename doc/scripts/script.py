from paraview.simple import *

s = GetActiveSource()
Hide(s)
Clip()
w = WarpByVector()
w.Vectors = "Normals"
Show()
