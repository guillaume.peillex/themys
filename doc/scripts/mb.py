from vtk.numpy_interface import dataset_adapter as dsa

# recover input
inp = inputs[0]

# Copy input into a new polydata
pd = vtk.vtkPolyData()
pdn = dsa.WrapDataObject(pd)
pd.DeepCopy(inp.VTKObject)

# needs to copy in order to preserve input points.
pts = numpy.copy(inp.Points)

# modify Y coordinate
pts[:, 1] = 1

# set new points on the output.
pdn.Points = pts

# Copy input into another new polydata
pd2 = vtk.vtkPolyData()
pd2n = dsa.WrapDataObject(pd2)
pd2.DeepCopy(inp.VTKObject)

# needs to copy in order to preserve input points.
pts2 = numpy.copy(inp.Points)

# modify Y coordinate
pts2[:, 2] = 1

# set new points on the output.
pd2n.Points = pts2

# Setup output
output.SetNumberOfBlocks(2)
output.SetBlock(0, pd)
output.SetBlock(1, pd2)
