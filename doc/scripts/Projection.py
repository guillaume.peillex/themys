# get the input poly data

pd1 = inputs[0]
pds1 = dsa.WrapDataObject(pd1)
pts = numpy.copy(polydata.Points)
pts[:, 1] = 1
pds1.Points = pts

pd2 = vtk.vtkPolyData()
pds2 = dsa.WrapDataObject(pd2)
pts2 = numpy.copy(polydata.Points)
pts2[:, 0] = 1
pds2.Points = pts2

output.SetNumberOfBlocks(2)
output.AddBlock(pds1.vtkObject)
output.AddBlock(pds2.vtkObject)
