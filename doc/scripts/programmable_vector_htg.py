# Compute a new scalar field CellData from a vector field CellData
# on HTG with a description of the interfaces for mixed cells

# Shallow copy input to output: Mesh, pointdata and celldata
output.ShallowCopy(inputs[0].VTKObject)

# Browse serialized blocks (materials or part of materials)
# Access to the components of a vector field requires looping
# over the blocks whereas it is not necessary on a scalar field
for block in output:
    try:
        # Get CellData "vtkInterfaceDistance"
        data = block.CellData["vtkInterfaceDistance"]

        # Operate dD
        dD = data[:, 1] - data[:, 0]

        # Extract third dimension on data2
        data2 = data[:, 2]

        # Build data3 in terms of data2 and dD
        data3 = where(data2 == 0, dD, 0.0)

        # In the case of the construction of an HTG with pit value,
        # the number of cells is not the size of the field.
        # The field must be resized to the size of the number of cells.
        nbCells = block.GetNumberOfCells()
        data4 = numpy.zeros(nbCells, dtype=numpy.float)
        nbVals = data3.shape[0]
        data4[:nbVals] = data3

        # Add new field
        block.CellData.append(data4, "Nouveau")
    except AttributeError:
        pass
