add_subdirectory(filters)
add_subdirectory(getstarting)
add_subdirectory(readers)
add_subdirectory(scripting)
add_subdirectory(tutos)
add_subdirectory(writers)

set(IMG_FILES
    ${IMG_FILES}
    PARENT_SCOPE)
