set(RAW_IMG_FILES
    ${RAW_IMG_FILES}
    ${CMAKE_CURRENT_LIST_DIR}/ScriptingTutorial0x.png
    ${CMAKE_CURRENT_LIST_DIR}/ScriptingTutorial1x.png
    ${CMAKE_CURRENT_LIST_DIR}/ScriptingTutorial2x.png
    ${CMAKE_CURRENT_LIST_DIR}/ScriptingTutorial3x.png
    ${CMAKE_CURRENT_LIST_DIR}/ScriptingTutorial4x.png
    ${CMAKE_CURRENT_LIST_DIR}/ScriptingTutorial5x.png
    ${CMAKE_CURRENT_LIST_DIR}/ScriptingTutorial6x.png
    PARENT_SCOPE)
