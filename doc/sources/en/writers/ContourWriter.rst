Documentation: `doc fr <../../fr/writers/ContourWriter.html>`__

Contour file writer in DAT format
=================================

Description
-----------

The **ContourWriter** filter is used to write a **.dat** type file containing
the set of contours of the different meshes present in the source
selected in the **Pipeline**.

Use
---

Writing is triggered by selecting **File->Save Data** in the GUI, or via
the corresponding keyboard shortcut (**CTRL^S**) or by clicking on [the icon](|image0|).

In the window that opens, select **Contour Files(*.dat)** in the field **Files of type:**

|image1|

Then, after having chosen the place where to write the contour file, you must enter the desired name (with or without extension)
then click on **OK**.

The .dat format
---------------

The **.dat** file format contains a list of polylines in an **ASCII** formatted file.

Each line of the file contains the coordinates of one of the points of a poly-line.

The poly-lines are separated by the character **&**, alone on a line.

All points in a polyline are connected together in the order they appear in the file.

Here is an example of a **.dat** file describing two poly-lines.

::

    -1.0000000000 0.0000000000
    0.0000000000 1.0000000000
    1.0000000000 0.0000000000
    0.0000000000 -1.0000000000
    &
    -2.0000000000 0.0000000000
    0.0000000000 2.0000000000
    2.0000000000 0.0000000000
    &

Each line describes:

* either a point in space by a tuple of two or three float values;

* or a **&** separator positioned at the end of the description of a poly-line.

The **CEAReaderDAT** filter is used to read this format.

Example
-------

The image below superimposes a non-simply connected mesh (gray) and the resulting contour (white line):

|image2|

The corresponding outline file is as follows:

::

    0.8000000119 0.5199999809 0.0000000000
    0.7799999714 0.5199999809 0.0000000000
    0.7799999714 0.5000000000 0.0000000000
    0.7799999714 0.4799999893 0.0000000000
    0.7599999905 0.4799999893 0.0000000000
    0.7599999905 0.4600000083 0.0000000000
    0.7400000095 0.4600000083 0.0000000000
    ...
    0.8199999928 0.5799999833 0.0000000000
    0.8199999928 0.5600000024 0.0000000000
    0.8199999928 0.5400000215 0.0000000000
    0.8000000119 0.5400000215 0.0000000000
    0.8000000119 0.5199999809 0.0000000000
    &
    0.5000000000 0.5400000215 0.0000000000
    0.5000000000 0.5199999809 0.0000000000
    0.4799999893 0.5199999809 0.0000000000
    0.4799999893 0.5400000215 0.0000000000
    0.5000000000 0.5400000215 0.0000000000
    &
    0.4399999976 0.5400000215 0.0000000000
    0.4399999976 0.5199999809 0.0000000000
    0.4199999869 0.5199999809 0.0000000000
    0.4199999869 0.5400000215 0.0000000000
    0.4399999976 0.5400000215 0.0000000000
    &
    0.4199999869 0.4600000083 0.0000000000
    0.4000000060 0.4600000083 0.0000000000
    0.4000000060 0.4799999893 0.0000000000
    0.4000000060 0.5000000000 0.0000000000
    0.4199999869 0.5000000000 0.0000000000
    0.4199999869 0.4799999893 0.0000000000
    0.4199999869 0.4600000083 0.0000000000
    &
    0.4399999976 0.5000000000 0.0000000000
    0.4600000083 0.5000000000 0.0000000000
    0.4600000083 0.4799999893 0.0000000000
    0.4600000083 0.4600000083 0.0000000000
    0.4600000083 0.4399999976 0.0000000000
    0.4799999893 0.4399999976 0.0000000000
    0.4799999893 0.4600000083 0.0000000000
    0.4600000083 0.4600000083 0.0000000000
    0.4399999976 0.4600000083 0.0000000000
    0.4399999976 0.4799999893 0.0000000000
    0.4399999976 0.5000000000 0.0000000000
    &
    0.6000000238 0.5000000000 0.0000000000
    0.6200000048 0.5000000000 0.0000000000
    0.6200000048 0.5199999809 0.0000000000
    0.6000000238 0.5199999809 0.0000000000
    0.6000000238 0.5000000000 0.0000000000
    &

There are 6 poly-lines, one for each of the contours of the mesh, illustrated in the figure below:

|image3|

.. |image0| image:: ../../img/writers/pqSave.svg
  :width: 16
.. |image1| image:: ../../img/writers/saveFile.png
  :width: 400
.. |image2| image:: ../../img/writers/example.png
  :width: 800
.. |image3| image:: ../../img/writers/example_contours.png
  :width: 800
