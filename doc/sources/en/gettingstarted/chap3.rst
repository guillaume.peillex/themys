Documentation: `doc fr <../../fr/gettingstarted/chap3.html>`__



Modify the properties
=====================

Si vous modifiez l’une des propriétés de la source de sphère, telles que les propriétés de la section **Properties**
du panneau **Properties**, y compris le *Radius* (rayon) du maillage sphérique ou son *Center* (centre), le bouton
**Apply** sera à nouveau mis en surbrillance. Une fois que vous avez terminé toutes les modifications de propriété,
vous pouvez cliquer sur **Apply** pour appliquer les modifications. Une fois les modifications appliquées, themys
réexécutera la source de sphère pour produire un nouveau maillage, comme demandé. Il mettra alors automatiquement
à jour la vue, et vous verrez le nouveau résultat rendu.

Si vous modifiez l’une des propriétés d’affichage de la source de sphère, telles que les propriétés sous la section
**Display** du panneau **Properties** (y compris **Representation** ou **Opacity**), le bouton **Apply** n’est pas
affecté, les modifications sont immédiatement appliquées et la vue est mise à jour.

La raison derrière cela est que, généralement, l’exécution de la source (ou du filtre) est plus intensive en calcul
que le rendu. La modification des propriétés source (ou filtre) entraîne la réexécution de cet algorithme, tandis
que la modification des propriétés d’affichage, dans la plupart des cas, ne déclenche qu’un nouveau rendu avec un
état graphique mis à jour.

.. note::
   **Themys** propose via le menu **Edit**>**Settings** d’exécuter automatiquement le bouton **Apply** (Auto Apply).
   Nous ne le recommandons pas.
