Documentation: `doc fr <../../fr/gettingstarted/chap1.html>`__



Getting started
===============

**Themys** est l’interface graphique de l’application **ParaView**. L’interface utilisateur est conçue pour vous permettre de créer facilement des pipelines pour le traitement des données avec une complexité arbitraire. L’interface utilisateur fournit des panneaux vous permettant d’inspecter et de modifier les pipelines, de modifier les paramètres qui affectent à leur tour les pipelines de traitement, d’effectuer diverses actions de sélection et d’inspection des données pour introspecter les données et de générer des rendus. Nous couvrirons divers aspects de l’interface utilisateur pour la majeure partie de ce guide.

Commençons par examiner les différents composants de l’interface utilisateur. Si vous exécutez themys pour la première fois, vous verrez quelque chose de similaire à la Fig. 1.1. L’interface utilisateur est composée de menus, de panneaux ancrables, de barres d’outils et de la fenêtre d’affichage, la partie centrale de la fenêtre de l’application.

|image0|

Fig. 1.1 Fenêtre d'application Themys

Les menus fournissent l’ensemble standard d’options typiques d’une application de bureau, y compris les options d’ouverture/enregistrement de fichiers (menu **File**), d’annulation/rétablissement (menu **Edit**), d'onglet et de visibilité de la barre d’outils (menu **View**). En outre, les menus permettent de créer des sources qui génèrent des jeux de données de test de différents types (menu **Sources**) ainsi que de nouveaux filtres pour le traitement des données (menu **Filters**). Le menu **Outils** permet d’accéder à certaines des fonctionnalités avancées de themys telles que la gestion des plugins et des favoris.

Les panneaux vous permettent de jeter un coup d’œil sur l’état de l’application.

Par exemple, vous pouvez inspecter le pipeline de visualisation qui a été configuré (**Pipeline Browser**, l'explorateur du pipeline), ainsi que la mémoire utilisée (**Memory Inspector**) et les paramètres ou propriétés d’un module de traitement (panneau **Properties**). Plusieurs panneaux vous permettent également de modifier les valeurs affichées, par exemple, le panneau **Properties** affiche non seulement les paramètres du module de traitement, mais il vous permet également de les modifier. Plusieurs des panneaux sont sensibles au contexte. Par exemple, le panneau **Properties** change pour afficher les paramètres du module sélectionné lorsque vous modifiez le module actif dans **Pipeline Browser**.

Les barres d’outils sont conçues pour fournir un accès rapide aux fonctionnalités courantes. Plusieurs des actions de la barre d’outils sont accessibles à partir d’autres emplacements, y compris les menus ou les panneaux. À l’instar des panneaux, certains boutons de la barre d’outils sont contextuels et seront activés ou désactivés en fonction du module ou de la vue sélectionnée.

La fenêtre d’affichage ou la partie centrale de la fenêtre **Themys**, similaire à la fenêtre **ParaView**, est la zone où **Themys** affiche les résultats générés à partir des données. Les conteneurs dans lesquels les données peuvent être rendues ou affichées sont appelés vues. Vous pouvez créer plusieurs types de vues différents, qui sont tous disposés dans cette zone de fenêtre d’affichage. Par défaut, une vue 3D est créée, qui est l’une des vues les plus couramment utilisées dans Themys.

Pour mieux comprendre le processus de visualisation à travers la façon d'utiliser l’interface de l’application, prenons un exemple simple : créer une source de données et lui appliquer un filtre.

.. |image0| image:: ../../img/getstarting/themys_gui.png
