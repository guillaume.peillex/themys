Documentation: `doc fr <../fr/index.html>`__

Welcome to Themys's documentation!
==================================

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   history/history
   gettingstarted/gettingstarted
   filters/filters
   tutos/tutos
   procedures/procedures
   readers/readers
   writers/writers

Summary
=======

* :ref:`search`
