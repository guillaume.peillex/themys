Documentation: `doc fr <../../fr/readers/HerculesReader_chap3_SpectralRangeIndices.html>`__



Spectral Range Indices
^^^^^^^^^^^^^^^^^^^^^^

Le chargement d'un champ de valeurs spectral est trés coûteux puisque pour chaque cellule on
peut avoir plusieurs centaines de valeurs.

C'est pourquoi chaque grandeur spectrale est proposée sous trois formes :

* sa **version complète vectorielle**, par exemple : *Milieu:Spectrum* ;
* sa **version partielle vecorielle**, par exemple : *vtkPart_Milieu:Spectrum* ;
* sa **version sommée partielle scalaire**, par exemple : *vtkSum_Milieu:Spectrum*.

Le premier est le champ de valeurs complétement décrit tel qu'il est stocké dans la base Hercule.

Les deux suivants permettent de réaliser une réduction au chargement d'un champ spectral.

Pour cela, les valeurs de la propriété **Specral Range Indices** est exploitée afin de déterminer les
indices qui seront retenues :

* soit pour l'extraction d'une partie du champ spectral, *vtkPart_* ;
* soit pour réaliser la somme (partielle), *vtkSum_*.

La première valeur indique de cette propriété indique le premier indice qui sera pris en compte.
La valeur -1 est équivalente à 0.

La seconde valeur indique de cette même propriété indique le dernier indice retenue.
S'il vaut -1, automagiquement il prendra en interne la valeur de l'indice correspondant au dernier élément
du champ spectral concerné.

.. warning::
   Les préfixes *vtkPart_* et *vtkSum_* pourraient prochainement être déplacé derrière la description de
   l'objet géométrique. C'est à dire *vtkPart_Milieu:Spectrum* deviendrait *Milieu:vtkPart_Spectrum*.
