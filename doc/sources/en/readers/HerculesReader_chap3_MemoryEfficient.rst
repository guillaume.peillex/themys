Documentation: `doc fr <../../fr/readers/HerculesReader_chap3_MemoryEfficient.html>`__



Memory efficient
^^^^^^^^^^^^^^^^

Cette propriété **Memory efficient** permet de réaliser un gain mémoire d'un facteur 2 lors du chargement d'un champ
de valeurs en transformant son type informatique de *double* à *float*.

L'activation de cette propriété n'est pris en charge que pour les nouveaux champs de valeurs chargés.
C'est tout particulièrement le cas lors d'un changement de temps de simulation.

.. note::
   N'oubliez pas ensuite de cliquer sur **Apply** (appliquer) dans **Properties** de l'instance du lecteur modifiée.
