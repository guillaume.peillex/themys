Documentation: `doc fr <../../fr/procedures/change_background_color.html>`__



Change la couleur de fond
=========================

Trouver les propriétés pour la couleur de fond
----------------------------------------------

Sélectionner un élément du “Pipeline Browser” en cliquant dessus.

Aller dans “Properties”.

1. Mode de recherche manuel

Passer en mode avancé en cliquant sur le bouton représentant une petite roue à la fin de la ligne d'édition ("Search for properties by name").

Se déplacer vers le bas à la recherche de la propriété nommée “Background”.

2. Mode de recherche automatique

Entrez dans la ligne d'édition de recherche d'une propriété par son nom ("Search for properties by name") les premières lettres de "Background" (sans saisir les guillemets), e.g. Ba.

Apparaît alors les propriétés commençant par ces premières lettres.

Changer la propriété couleur du fond
------------------------------------

Differents modes sont proposés.
Le plus couramment utilisé est la couleur simple ( “Single Color”).

Le choix de la couleur se fait en cliquant sur le bouton « Color » juste en-dessous afin de retrouver la couleur actuellement sélectionnée dans une représentation des couleurs sous forme de disque.

Il vous suffit de choisir une autre couleur.
