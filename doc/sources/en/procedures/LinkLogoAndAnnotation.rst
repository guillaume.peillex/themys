Documentation: `doc fr <../../fr/procedures/LinkLogoAndAnnotation.html>`__



How to link logo with annotation text ?
=======================================

With Paraview, it is possible to link several objects together, to
create a composition of objects for 2D objects in the foreground.

This is for example the case of an image representing a logo and a
result of the PythonAnnotation filter.

The idea is then to be able to move them together in order to adjust
their positioning in the view.

Procedure
---------

Here’s how to link an image and text:

-  Open ParaView ;
-  Create a text: Menu Source -> Texte
-  Entrer the follow text: " Text"
-  Choose the position text: TextPosition -> LowerLeftCorner
-  Create an image to define a logo: Menu Source -> Logo
-  Load a picture
-  Create a link: Menu Tools -> Manage Links -> Add -> Mode:
   PropertyLink -> TextSourceRepresentation:Position ->
   LogoSourceRepresentation:Position -> Ok
-  From now on, logo and text are now moving together.
