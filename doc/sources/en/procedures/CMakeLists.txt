set(DOC_FILES_EN
    ${DOC_FILES_EN}
    ${CMAKE_CURRENT_LIST_DIR}/axes.rst
    ${CMAKE_CURRENT_LIST_DIR}/calculators.rst
    ${CMAKE_CURRENT_LIST_DIR}/change_background_color.rst
    ${CMAKE_CURRENT_LIST_DIR}/clipping.rst
    ${CMAKE_CURRENT_LIST_DIR}/contouring.rst
    ${CMAKE_CURRENT_LIST_DIR}/custom_filter.rst
    ${CMAKE_CURRENT_LIST_DIR}/ExportProfile.rst
    ${CMAKE_CURRENT_LIST_DIR}/ExportSelection.rst
    ${CMAKE_CURRENT_LIST_DIR}/field_vector.rst
    ${CMAKE_CURRENT_LIST_DIR}/find_cell.rst
    ${CMAKE_CURRENT_LIST_DIR}/FixeATimeForOneBase.rst
    ${CMAKE_CURRENT_LIST_DIR}/Histogram.rst
    ${CMAKE_CURRENT_LIST_DIR}/InterfaceModes.rst
    ${CMAKE_CURRENT_LIST_DIR}/LinkLogoAndAnnotation.rst
    ${CMAKE_CURRENT_LIST_DIR}/PlotOverLineCustom.rst
    ${CMAKE_CURRENT_LIST_DIR}/procedures.rst
    ${CMAKE_CURRENT_LIST_DIR}/PythonAnnotation.rst
    ${CMAKE_CURRENT_LIST_DIR}/rescale_range_mode.rst
    ${CMAKE_CURRENT_LIST_DIR}/SelectionByGlobalID.rst
    ${CMAKE_CURRENT_LIST_DIR}/SelectionDisplay.rst
    ${CMAKE_CURRENT_LIST_DIR}/SelectionWithGeometry.rst
    ${CMAKE_CURRENT_LIST_DIR}/Slicing.rst
    ${CMAKE_CURRENT_LIST_DIR}/Symmetry.rst
    ${CMAKE_CURRENT_LIST_DIR}/Thresholding.rst
    ${CMAKE_CURRENT_LIST_DIR}/TimeManagement.rst
    ${CMAKE_CURRENT_LIST_DIR}/Transform.rst
    ${CMAKE_CURRENT_LIST_DIR}/use_separate_color_map.rst
    PARENT_SCOPE)
