Documentation: `doc fr <../../fr/procedures/Symmetry.html>`__



Apply a reflection
==================

Description
-----------

Applying a reflection on a data set produces a symmetrical copy of it
about a given plane. This operation is useful for instance to
reconstruct a mesh based on a half or a quarter of a structure.

Procedure using the GUI
-----------------------

Start by selecting the data set to reflect from the ``Pipeline Browser``
panel by clicking on it. Choose one of the reflection filters in the
filter toolbar, named ``ReflectX``, ``ReflectY`` and ``ReflectZ``,
according to the axis orthogonal to the reflection plane.

|image0|

Click on the ``Apply`` button in the ``Properties`` panel to execute the
filter. The output will be the reflected dataset about the selected
reflection plane.

|image1|

To preserve the input data set on top of its reflection, tick the
``Copy Input`` checkbox. To gain access to this advanced option, click
on the ``Advanced Properties`` wheel button illustrated below.

|image2|

Lastly, the plane can be moved along its axis by adjusting the
``Position`` value in the ``Properties`` panel, as shown below.

|image3|

Procedure using Python scripting
--------------------------------

.. code:: py

   # Find the source data set with its name
   # Replace 'blow.vtk' according to the data set to reflect
   mySource = FindSource('blow.vtk')

   # Create a new 'ReflectX' filter
   # Likewise for 'ReflectY' and 'ReflectZ'
   reflectX = ReflectX(Input=mySource)

   # Optionally enable CopyInput
   reflectX.CopyInput = 1

   # Optionally modify the reflection plane position
   reflectX.Position = 5.0

   # Get active render view
   renderView = GetActiveViewOrCreate('RenderView')

   # Show output data
   reflectXDisplay = Show(reflectX, renderView)

   # Hide original source data
   Hide(mySource, renderView)

   # Show color bar
   reflectXDisplay.SetScalarBarVisibility(renderView, True)

.. |image0| image:: ../../img/procedures/01BeforeReflection.png
.. |image1| image:: ../../img/procedures/01ApplyReflection.png
.. |image2| image:: ../../img/procedures/01ReflectionCopyInput.png
.. |image3| image:: ../../img/procedures/01ReflectionPosition.png
