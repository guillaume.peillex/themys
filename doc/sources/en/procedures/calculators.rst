Documentation: `doc fr <../../fr/procedures/calculators.html>`__



Define a new data array based on existing arrays
================================================

Description
-----------

Creating new data arrays based on existing ones is common to compute new
quantities, e.g. such as energy from mass and velocity. This can be
achieved using calculators.

Procedure using the GUI
-----------------------

1. Calculator
~~~~~~~~~~~~~

Start by selecting the data set from the ``Pipeline Browser`` panel by
clicking on it. Then click on the ``Calculator`` filter in the filter
toolbar as shown below.

|image0|

This filter creates a new array based on a mathematical expression which
can optionally include existing arrays. The calculator is similar in
appearance to a scientific calculator. Buttons for basic functions are
available, although the result expression can be entered manually as
well. Existing arrays can be conveniently added with the ``Scalars`` and
``Vectors`` combo boxes. Note that these contain point coordinates as
well. You can also choose whether to produce a point or cell array using
the ``Attribute Type`` combo box.

After clicking on ``Apply``, the result array is available with the
given name and can be manipulated as usual. For example, the image below
shows the computation of the norm of the data set points.

|image1|

**Tip:** Multiplying a field vector by ``iHat`` (resp. ``jHat`` and
``kHat``) extracts the first (second and third) scalar component of this
vector. Thereby, operator ``mag(vel)`` is equivalent to
``sqrt((vel*iHat)^2+(vel*jHat)^2+(vel*kHat)^2)`` and allows to calculate
the magnitude of a vector, its length, its euclian norm, norm 2.

2. Python Calculator
~~~~~~~~~~~~~~~~~~~~

The Python calculator is very similar to the standard calculator, but is
based on Python. For instance, specific array elements can be accessed
with brackets, as well as exclusive functions. For more details about
available functionalities, see `this documentation
page <https://docs.paraview.org/en/latest/UsersGuide/filteringData.html#python-calculator>`__.

Start by activating the ``Advanced Mode`` by clicking on the
corresponding button shown below. After selecting the data set from the
``Pipeline Browser`` panel by clicking on it, go into
``Filters > Search...`` in the menu.

|image2|

This opens a popup which you can type into. Start typing the filter name
``Python Calculator``, then select it in the list and press ``Enter``
when it is highlighted.

|image3|

Choose with the ``Array Association`` combo box whether to create a
point or cell array. Then, enter a Python expression in the
``Expression`` box. When the expression is well defined, click on
``Apply`` to compute the new array.

|image4|

**Tip:** In Python Calcultor, there is no ``iHat`` (resp. ``jHat`` and
``kHat``). Thereby, operator ``mag(vel)`` is equivalent to
``sqrt(dot(vel, vel)`` and allows to calculate the magnitude of a
vector, its length, its euclian norm, norm 2.

a. Functions working on data arrays
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

=========== ========= ======= ====== ========== ===== ===========
Name        Supported operand types  # Operands Data  association
=========== ========= ======= ====== ========== ===== ===========
abs         scalar    vector  tensor 1          point cell
cross                 vector         2          point cell
curl                  vector         1          point cell
det                           tensor 1          point cell
determinant                   tensor 1          point cell
dot         scalar    vector         2          point cell
eigenvalue                    tensor 1          point cell
eigenvector                   tensor 1          point cell
global_mean scalar    vector  tensor 1          point cell
global_max  scalar    vector  tensor 1          point cell
global_min  scalar    vector  tensor 1          point cell
gradient    scalar    vector         1          point cell
inverse                       tensor 1          point cell
laplacian   scalar                   1          point cell
ln          scalar    vector  tensor 1          point cell
log(==ln)   scalar    vector  tensor 1          point cell
log10       scalar    vector  tensor 1          point cell
max         scalar    vector  tensor 1          point cell
min         scalar    vector  tensor 1          point cell
mean        scalar    vector  tensor 1          point cell
mag         scalar    vector         1          point cell
norm        scalar    vector         1          point cell
strain                vector         1          point cell
trace                         tensor 1          point cell
vorticity             vector         1          point cell
=========== ========= ======= ====== ========== ===== ===========

Example: ``mag(vel)``

b. Functions working on dataset
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Name Supported Cell Types Data Association

============== === ==== === === ================
Name           Tri Quad Tet Hex Data association
============== === ==== === === ================
area           X   X            cell
aspect         X   X    X       cell
aspect_gamma            X       cell
diagonal                    X   cell
jacobian           X    X   X   cell
max_angle      X   X            cell
min_angle      X   X            cell
shear              X            cell
skew               X        X   cell
surface_normal X                cell
volume                  X   X   cell
vertex_normal  X                point
============== === ==== === === ================

c. Examples
^^^^^^^^^^^

Compute area for each cell
''''''''''''''''''''''''''

``area(inputs[0])`` and array association set to Cell Data

|image5|

Compute vector length for each point
''''''''''''''''''''''''''''''''''''

``sqrt(dot(BrownianVectors,BrownianVectors))`` or equivalently
``mag(BrownianVectors)`` where ``BrownianVectors`` is an array of 3D
vectors that are associated with each point in the dataset. Array
association set to Point Data.

Third example
'''''''''''''

``max(abs(trace(inverse(gradient(Normals)))))`` Normals is an array of
3D vectors that are associated to each point in the dataset.

|image6|

The expression first computes for each component of the normal vector
its gradient. It generates a tensor and computes the reverse of the
generated matrix. Then it sums everything on the diagonal of the reverse
matrix and computes the absolute value of the sum. Finally, it looks for
the maximum of all the absolute values.

**WARNING** **The numpy operators (section a) work on 1D arrays without
taking into account the topology of the mesh. In fact the calculations
requiring the neighborhood such as the gradient, exploit the neighboring
values ​​which are nearby (before or after) in memory.**

Procedure using Python scripting
--------------------------------

.. _calculator-1_en:

1. Calculator
~~~~~~~~~~~~~

.. code:: py

   # Find the source data set with its name
   # Replace 'Wavelet1' according to the desired data set
   mySource = FindSource('Wavelet1')

   # Create a new 'Calculator' filter
   calculator = Calculator(Input=mySource)

   # If needed, change array type to 'Point Data' or 'Cell Data'
   calculator.AttributeType = 'Point Data'

   # Define expression for output array
   calculator.Function = 'sqrt(coordsX^2+coordsY^2+coordsZ^2)'

   # Set output array name
   calculator.ResultArrayName = 'Norm'

   # Get active render view
   renderView = GetActiveViewOrCreate('RenderView')

   # Show output data
   calculatorDisplay = Show(calculator, renderView)

   # Hide original source data
   Hide(mySource, renderView)

   # Show color bar
   calculatorDisplay.SetScalarBarVisibility(renderView, True)

.. _python-calculator-1_en:

2. Python Calculator
~~~~~~~~~~~~~~~~~~~~

.. code:: py

   # Find the source data set with its name
   # Replace 'Wavelet1' according to the desired data set
   mySource = FindSource('Wavelet1')

   # Create a new 'PythonCalculator' filter
   pyCalc = PythonCalculator(Input=mySource)

   # If needed, change array type to 'Point Data' or 'Cell Data'
   pyCalc.ArrayAssociation = 'Point Data'

   # Define expression for output array
   pyCalc.Expression = 'sin(RTData)'

   # Set output array name
   pyCalc.ArrayName = 'Result'

   # Get active render view
   renderView = GetActiveViewOrCreate('RenderView')

   # Show output data
   pyCalcDisplay = Show(pyCalc, renderView)

   # Hide original source data
   Hide(mySource, renderView)

   # Show color bar
   pyCalcDisplay.SetScalarBarVisibility(renderView, True)

.. |image0| image:: ../../img/procedures/10Calculator.png
.. |image1| image:: ../../img/procedures/10CalculatorUsage.png
.. |image2| image:: ../../img/procedures/10FiltersMenu.png
.. |image3| image:: ../../img/procedures/10FilterSearch.png
.. |image4| image:: ../../img/procedures/10PythonCalculatorUsage.png
.. |image5| image:: ../../img/procedures/PythonCalculator_1.png
.. |image6| image:: ../../img/procedures/PythonCalculator_2.png
