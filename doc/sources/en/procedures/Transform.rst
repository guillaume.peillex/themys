Documentation: `doc fr <../../fr/procedures/Transform.html>`__



Transform
=========

Description
===========

Le filtre de **Transform** permet d’appliquer certains types ’opérations géométriques :
- translation (**Scale**),
- rotation (**Rotate**),
- agrandissement/réduction (**Translate**).

Dans sa version actuelle (2021), un seul traitement par type d’opération
est possible et l’ordre d’application est d’abord :

- agrandissement/réduction (si activé), puis
- rotation (si activé), puis
- translation (si activé).

Chaque application du filtre de **Transform** réalise une **Shallow Copy** (peu coûteuse en mémoire) du maillage mais forcément produit de nouvelles coordonnées (coût mémoire 3x le nombre de points).
C’est pourquoi, sur des gros maillages, il est conseillé de ne pas multiplier l’usage de ce filtre.

Exemple : translation puis agrandissement/réduction
===================================================

Dans les faits, si l’on souhaite effectuer une translation (**Translate**) puis un agrandissement/réduction (**Scale**), deux options sont possibles :
- la première, plus humainement compréhensible, propose d’appliquer le filtre **Transform** en appliquant qu’une transformation géométrique à la fois, et ceci autant de fois qu’il y a d’opérations (ici 2) ;
- la seconde, plus efficace mais plus difficile à mettre en place, consiste à renseigner le filtre **Transform** afin d’appliquer les deux opérations en même temps.

La première proposition est deux fois plus consommatrices en mémoire que la seconde.

Voyons, concrétement sur un exemple, comment appliquer ces deux options à partir du chargement du maillage décrivant le tristement célèbre volcan Sainte-Hélène. L’objet est ici de “normaliser” le maillage :
- en le translatant afin que le point en position minimale soit l’origine du repère ; et
- en appliquant un facteur de réduction afin que la largeur du maillage suivant l’axe X vale 1.

|image0| Figure 1: A gauche l’option 1 avec l’application de deux filtres **Transform**, à droite l’option 2 avec l’application d’un unique filtre **Transform**.

Proposition 1 : **Trasnform: Translate** puis **Trasnform: Scale**
------------------------------------------------------------------

Cette première option est plus humainement compréhensible puisque l’on va construire un premier filtre pour gérer la translation puis un second filtre pour gérer le facteur d’échelle.

Concernant le premier filtre, les valeurs à saisir au niveau de la description de la translation sont les suivantes :

::

   Translate -557945; -5.1079915e6; -682

et les autres valeurs restent inchangées:

::

   Rotate 0; 0; 0
   Scale 1;1;1

|image1|

Concernant, le second filtre, les valeurs à saisir au niveau du facteur d’échelle sont la suivante :

::

   Scale  0.000102249; 0.000102249; 1

et les autres valeurs restent inchangées:

::

   Translate 0;0;0
   Rotate 0; 0; 0

|image2|

Automatiquement, le filtre active est le dernier **Transform**.
Cliquer sur **Show Box** pour ne plus visualiser les interacteurs.
En cliquant sur **Data Axes Grid** dans **Properties**, nous faisons afficher la grille d’axes.

En y regardant de plus près, on constate les coûts mémoires suivant :
- pour le filtre de lecture/source est de 597 Ko ;
- pour le filtre de **Transform** est de 4 177 Ko (x7) !

La raison est assez simple, le filtre de lecture retourne un maillage **Image** avec des coordonnées implicites alors que l’application du filtre de **Transform** retourne un maillage **Structured (Curvilenear) Grid** avec des coordonnées explicites.

Proposition 2 : **Trasnform Translate/Scale**
---------------------------------------------

Cette seconde option est plus efficace en mémoire mais nécessite de modifier les paramètres naturelles des transformations.

En l’occurence, ici la translation étant appliquée après le facteur d’échelle (choix implicite du filtre **Transform**), les valeurs associées à la translation doivent subir cette transformation.

Ainsi, au lieu de saisir dans le filtre les valeurs suivantes directement déduites de l’onglet **Information** du filtre de lecture :

::

   Translate -557945; -5.1079915e6; -682
   Rotate 0; 0; 0
   Scale  0.000102249; 0.000102249; 1

il faut appliquer le facteur d’échelle (multiplier le paramètre de translation par celui du facteur d’échelle) sur les paramètres de la translation pour finalement saisir ces nouveaux paramètres du filtre :

::

   Translate -57.0493; -522.287; -682
   Rotate 0; 0; 0
   Scale  0.000102249; 0.000102249; 1

|image3|

Le coût de ce filtre **Transform** reste inchangé à 4 177 Ko.

TODO Transform faire évoluer avant de décrire dans l’ordre les transformations unitaires que l’on souhaite appliquer

.. |image0| image:: ../../img/procedures/Transform.png
.. |image1| image:: ../../img/procedures/Transform_option1_a.png
.. |image2| image:: ../../img/procedures/Transform_option1_b.png
.. |image3| image:: ../../img/procedures/Transform_option2.png
