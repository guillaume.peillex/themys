Documentation: `doc en <../../en/readers/HerculesReader_chap3_MeshArray.html>`__



Mesh Array
^^^^^^^^^^

La propriété **Mesh Array** liste les différents maillages de simulation qui ont été écrits par le
code de simulation. L'utilisateur peut ainsi sélectionner les maillages de simulation
qui lui semblent d'intérêt pour l'analyse à réaliser.

Ces maillages peuvent représenter différents aspects de la simulation allant du maillage
*d'éléments finis classique* de la simulation, de plans de coupe, de surfaces, de courbes, de particules
ou encore de rayons.

Contrairement à LOVE, aucune activation n'est nécessaire pour voir apparaître des contenus cachés de la base.
Dans Themys, il a été décidé de restituer l'intégralité du contenu de la base.

L'utilisateur devra ensuite procéder à d'autres sélections au niveau des matériaux
`Material Array (sélection des matériaux) <HerculesReader_chap3_MaterialArray.html>`_ puis des champs
de valerus `Cell and Point Data Array (sélection de champ de valeurs) <HerculesReader_chap3_DataArray.html>`_.
Bien entendu, ces choix devront être cohéents. Ce n'est qu'à l'issue de toutes ces sélections positionnées
que l'utilisateur pourra confirmer cet choix et déclencher le chargement en cliquant sur **Apply**.

A tout moment, l'utilisateur peut revenir sur ses choix.

.. warning::
   Il est conseillé de sélectionner le juste nécessaire car c'est ce qui sera chargé lors d'un changement de
   temps de simulation comme lors d'un parcours temporel pour les services proposés par la **GUI Themys**
   de type *over times*.
