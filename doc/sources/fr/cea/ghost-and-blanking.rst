(source 2015 :
https://blog.kitware.com/ghost-and-blanking-visibility-changes/ de Dan
Lipsa and Berk Geveci)

[[*TOC*]]

Motivation
==========

Les **cellules fantômes** sont des couches de cellules aux limites des
éléments d’un jeu de données. Ces cellules sont utilisées par des
algorithmes parallèles de données, pour assurer l’exactitude de leurs
résultats. Par exemple, considérons un algorithme qui calcule les faces
externes d’un ensemble de données. Considérez également que l’algorithme
s’exécute en parallèle sur plusieurs nœuds, chaque nœud traitant une
partie du jeu de données d’origine. Cette opération produirait des faces
incorrectes aux limites des pièces. Les cellules fantômes peuvent
empêcher cela. Voir l’\ `entrée wiki
suivante <http://www.vtk.org/Wiki/VTK/Parallel_Pipeline>`__ pour une
description plus détaillée des cellules fantômes.

Les cellules de masquage sont utilisées pour spécifier que certains
éléments d’une grille ne font pas partie des données. Par exemple, un
jeu de données régulier avec un trou peut être spécifié en masquant les
cellules qui recouvrent le trou. Les cellules de suppression sont prises
en charge pour vtkStructuredGrid et vtkUniformGrid.

Nous modifions la manière dont les cellules fantômes et masquées sont
stockées en :

-  meilleur masquage du support: dans le passé, les cellules masquées
   étaient stockées en tant que membres de vtkStructuredGrid et
   vtkUniformGrid. Cela a créé le besoin d’un traitement spécial lorsque
   ces tableaux devaient être passés entre des algorithmes ou des
   processus. Maintenant, les cellules vides sont marquées d’un bit dans
   un tableau de champs. Les tableaux de champs sont transmis
   automatiquement entre les algorithmes et les processus
-  fournir une compatibilité binaire avec VisIt: VTK utilise désormais
   les `mêmes bits que
   VisIt <http://www.visitusers.org/index.php?title=Representing_ghost_data>`__
   pour marquer les cellules fantômes et les cellules vides
-  économiser de l’espace: les tableaux pour stocker le blanking ne sont
   plus nécessaires

Notez qu’il existe des fantômes et des masques pour les cellules et les
points. Remplacez simplement la cellule par le point pour obtenir la
description correspondante des fantômes de point et de la suppression

Aperçu des modifications
========================

Pour atteindre nos objectifs, nous avons apporté les changements
suivants:

-  Auparavant, les cellules fantômes étaient marquées dans un tableau
   d’attributs char non signé appelé «vtkGhostLevels». Pour chaque
   cellule, le niveau fantôme a été stocké à l’identifiant de cellule
   correspondant. Désormais, il n’y a pas de distinction entre les
   niveaux fantômes. Les cellules à n’importe quel niveau fantôme sont
   marquées en définissant le bit vtkDataSetAttributes :: DUPLICATECELL
   à l’ID de cellule correspondant, dans le tableau d’attributs char non
   signé appelé vtkDataSetAttributes :: GhostArrayName ()
   («vtkGhostType»).
-  Auparavant, les filtres supprimaient toutes les cellules fantômes
   qu’ils demandaient en amont avant de finaliser la sortie. Cela n’est
   plus fait. L’utilisateur peut supprimer toutes les cellules fantômes
   à la fin du traitement du pipeline, si nécessaire. Notez que les
   cellules fantômes ne sont pas affichées dans la fenêtre de rendu et
   ne consomment qu’une petite quantité de mémoire par rapport à
   l’ensemble de données, ce qui signifie que leur suppression n’est
   souvent pas nécessaire.
-  vtkUniformGrid et vtkStructuredGrid prenaient auparavant en charge le
   masquage via les tableaux de membres CellVisibility et
   PointVisibility. Ces tableaux sont supprimés et la fonctionnalité de
   suppression est prise en charge en définissant le bit
   vtkDataSetAttributes :: HIDDENCELL à l’ID de cellule correspondant
   dans le tableau d’attributs «vtkGhostType».
-  Nous augmentons la version de fichier pour les fichiers VTK Legacy
   (voir VTK / IO / Legacy) à 4.0 (à partir de 3.0). Nous augmentons la
   version de fichier pour les fichiers XML VTK (voir VTK / IO / XML) à
   2.0 (de 0.1 pour les fichiers utilisant le type d’en-tête UInt32 et
   de 1.0 pour les fichiers utilisant le type d’en-tête UInt64). Nous
   augmentons ces versions car les cellules fantômes sont désormais
   stockées dans un fichier utilisant le nouveau format vtkGhostType
   décrit dans la section précédente. Les nouveaux lecteurs lisant
   d’anciens fichiers convertissent un tableau d’attributs
   vtkGhostLevels en un tableau vtkGhostType. Cela préserve les cellules
   fantômes écrites à l’aide d’anciens lecteurs.

Bits utilisés dans le tableau de type fantôme
=============================================

Nous utilisons les bits suivants pour stocker des informations sur
chaque cellule de l’ensemble de données. Ces bits sont
`compatibles <http://www.visitusers.org/index.php?title=Representing_ghost_data>`__
avec Visit.

**vtkDataSetAttribute::DUPLICATECELL** spécifie que cette cellule est
présente sur plusieurs processeurs. Ceci est une cellule fantôme. Cette
cellule n’est pas rendue. Il n’est présent que pour aider à obtenir des
résultats corrects lors du traitement d’un seul sous-ensemble des
données d’origine. Les attributs associés à ces cellules sont valides,
ils peuvent donc être utilisés dans certaines opérations statistiques
telles que min / max. Les cellules fantômes ne doivent pas être
utilisées dans des opérations telles que la moyenne, car cela
entraînerait le comptage de la même cellule plusieurs fois. Une
description similaire s’applique à
**vtkDataSetAttribute::DUPLICATEPOINT**

**vtkDataSetAttribute::REFINEDCELL** spécifie que d’autres cellules sont
présentes qui affinent cette cellule. Comme la grille plus grossière est
souvent une grille structurée, il est difficile de supprimer les
cellules qui sont encore affinées. Au lieu de cela, ce bit est utilisé
pour marquer ce type de cellules. Les valeurs associées à cette cellule
peuvent être utilisées dans l’interpolation. Notez qu’il n’y a pas de
vtkDataSetAttribute :: REFINEDPOINT. Pour plus d’informations sur les
ensembles de données AMR dans VTK, consultez `Visualisation et analyse
des ensembles de données
AMR <https://blog.kitware.com/visualization-analysis-of-amr-datasets/>`__
dans La Source.

**vtkDataSetAttribute::HIDDENCELL** spécifie que cette cellule ne fait
pas partie du modèle, elle est uniquement utilisée pour maintenir la
connectivité de la grille. Ceci est une cellule vide. Il n’est pas rendu
et toutes les valeurs qui lui sont associées sont ignorées. Une
description similaire s’applique à **vtkDataSetAttribute::HIDDENPOINT**

Les autres bits spécifiés par VisIt ne sont pas utilisés dans VTK.

Modifications de l’API
======================

Pour tester si une cellule est une cellule fantôme, précédemment, nous
avons utilisé **grid-> GetCellData () -> GetArray (“vtkGhostLevels”) ->
GetValue (cellId)> 0**. C’est parce que nous avons stocké les niveaux
fantômes dans le tableau fantôme. Maintenant, nous utilisons **grid->
GetCellGhostArray () -> GetValue (cellId) & vtkDataSetAttributes ::
DUPLICATECELL**.

Pour spécifier qu’une cellule est une cellule fantôme, au lieu de
**grid-> GetCellData () -> GetArray (“vtkGhostLevels”) -> SetValue
(cellId, ghostLevel)**, utilisez: **vtkUnsignedCharArray \* ghosts =
grid-> GetCellGhostArray (); ghosts-> SetValue (cellId, ghosts->
GetValue (cellId) \| vtkDataSetAttributes :: DUPLICATECELL);** Notez que
nous utilisons une opération «ou» pour conserver les autres bits qui
pourraient être définis pour la cellule.

Pour créer un nouveau tableau fantôme, au lieu de **vtkDataSetAttributes
:: GenerateGhostLevelArray, utilisez vtkDataSetAttributes ::
GenerateGhostArray**. Nous avons changé le nom car il n’y a pas de
niveaux fantômes dans le nouveau tableau fantôme.

Pour tester s’il y a des cellules fantômes, au lieu de **grid->
GetCellData () -> GetArray (“vtkGhostLevels”)! = NULL**, utilisez
**grid-> HasAnyGhostCells ()**. Apportez des modifications similaires
pour tester s’il y a des cellules vides. Nous avons effectué ce
changement car auparavant un tableau fantôme non NULL signifiait qu’il y
avait des cellules fantômes, maintenant cela signifie qu’il y a des
cellules vides ou des cellules fantômes.

Pour obtenir un pointeur vers le tableau fantôme, au lieu de **grid->
GetCellData () -> GetArray («vtkGhostLevels»)**, utilisez **grid->
GetCellGhostArray ()**. Cette nouvelle fonction évite les comparaisons
de chaînes O (N) en mettant en cache le pointeur du tableau fantôme.

Nous supprimons un paramètre ghostLevel de **vtkPolyData ::
RemoveGhostCells**, **vtkUnstructuredGrid :: RemoveGhostCells**,
**vtkGlyph3D :: Execute**, **vtkDataSetSurfaceFilter ::
UnstructuredGridExecute**. En effet, nous ne supprimons plus les niveaux
fantômes demandés par un filtre après la fin de l’exécution du filtre.

Nous supprimons **GetCellVisibilityArray** de **vtkStructuredGrid** et
**vtkUniformGrid**. Utilisez plutôt **GetCellGhostArray** et utilisez le
bit **vtkDataSetAttributes :: HIDDENCELL** pour définir ou tester si une
cellule est vide.

Nous ajoutons un paramètre supplémentaire à **vtkDataReader ::
ReadFieldData**, **vtkXMLDataReader :: ReadArrayValues**,
**vtkXMLStructuredDataReader :: ReadSubExtent** qui spécifie si nous
lisons des données de cellule ou de point. Ceci est nécessaire pour que
nous puissions utiliser les bits appropriés lors de la conversion des
niveaux fantômes.

Nous supprimons **vtkStructuredGridWriter ::: WriteBlanking** car la
suppression est désormais écrite lors de l’enregistrement du tableau
**vtkGhostType**.

Mise à jour de la version du fichier XML VTK (16/04/2015)
=========================================================

Les utilisateurs disposant d’un ancien VTK ne pourront pas lire les
fichiers XML VTK générés avec un nouveau VTK contenant les modifications
fantômes décrites, même si ces fichiers ne contiennent pas de cellules
fantômes ou de suppression. C’est parce que nous incrémentons la version
du fichier XML VTK.

Pour résoudre ce problème, nous utilisons la version de fichier
précédente pour les fichiers XML VTK (0.1 pour les fichiers utilisant le
type d’en-tête UInt32 et 1.0 pour les fichiers utilisant le type
d’en-tête UInt64) à moins que les données ne soient structurées ou
structurées en grille et qu’il existe un tableau vtkGhostType.
