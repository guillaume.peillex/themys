Cette documentation est fortement inspiré des pages sur le
WrapDataObject du blog
https://blog.kitware.com/improved-vtk-numpy-integration/.

En 2014, KitWare a introduit un nouveau module Python appelé
numpy_interface dans VTK afin de faciliter l’interface VTK et **numpy**.
En voici un exemple.

Les lignes préfixées par ‘>>’ correspondent aux affichages de résultats
obtenus à l’issu des instructions décritent dans les lignes sans ce
préfixe. Cette décision de présenter les exemples ainsi est opposée à
celle que l’on retrouve généralement dans la littérature s’inspirant du
prompt ou invite des commandes systémes. Notre façon de faire à
l’avantage de faciliter le copier/coller des instructions afin de
reproduire les exemples que l’on donne ici.

.. code:: python

   >>> import vtk
   >>> from vtk.numpy_interface import dataset_adapter as dsa
   >>> from vtk.numpy_interface import algorithms as algs
   >>> s = vtk.vtkSphereSource()
   >>> e = vtk.vtkElevationFilter()
   >>> e.SetInputConnection(s.GetOutputPort())
   >>> e.Update()
   >>> sphere = dsa.WrapDataObject(e.GetOutput())
   >>> print sphere.PointData.keys()
   ['Normals', 'Elevation']
   >>> print sphere.PointData['Elevation']
   [ 0.5         0.          0.45048442  0.3117449   0.11126047  0.          0.
     0.          0.45048442  0.3117449   0.11126047  0.          0.          0.
     0.45048442  0.3117449   0.11126047  0.          0.          0.
     0.45048442  0.3117449   0.11126047  0.          0.          0.
     0.45048442  0.3117449   0.11126047  0.          0.          0.
     0.45048442  0.3117449   0.11126047  0.          0.          0.
     0.45048442  0.3117449   0.11126047  0.          0.          0.
     0.45048442  0.3117449   0.11126047  0.          0.          0.        ]

Notez comment cette nouvelle API permet d’accèder aux données sur les
points et tout particulièrement au tableau d’élévation.

Notez également que lorsque l’on affiche le tableau Elevation, la sortie
ne ressemble pas à celle d’un vtkDataArray. En réalité:

.. code:: python

   >>> elevation = sphere.PointData['Elevation']
   >>> print type(elevation)
   <class 'vtk.numpy_interface.dataset_adapter.VTKArray'>
   >>> import numpy
   >>> print isinstance(elevation, numpy.ndarray)
   True

ce qui sous entend que le tableau retourné est un tableau numpy.

.. code:: python

   >>> sphere.PointData.append(elevation + 1, 'e plus one')
   >>> print algs.max(elevation)
   0.5
   >>> print algs.max(sphere.PointData['e plus one'])
   1.5
   >>> print sphere.VTKObject
   vtkPolyData (0x7fa20d011c60)
     ...
     Point Data:
       ...
       Number Of Arrays: 3
       Array 0 name = Normals
       Array 1 name = Elevation
       Array 2 name = e plus one

Tout est dans le module **numpy_interface**. Il lie les ensembles de
données VTK et les tableaux de données à des tableaux **numpy** et
introduit un certain nombre d’algorithmes qui peuvent fonctionner sur
ces objets. Ce module est riche et offre d’autres aspects d’intérêt
comme

.. code:: python

   >>> w = vtk.vtkRTAnalyticSource()
   >>> t = vtk.vtkDataSetTriangleFilter()
   >>> t.SetInputConnection(w.GetOutputPort())
   >>> t.Update()
   >>> ugrid = dsa.WrapDataObject(t.GetOutput())
   >>> print algs.gradient(ugrid.PointData['RTData'])
   [[ 25.46767712   8.78654003   7.28477383]
    [  6.02292252   8.99845123   7.49668884]
    [  5.23528767   9.80230141   8.3005352 ]
    ...,
    [ -6.43249083  -4.27642155  -8.30053616]
    [ -5.19838905  -3.47257614  -7.49668884]
    [ 13.42047501  -3.26066017  -7.28477287]]

Veuillez noter que cet exemple n’est pas très facilement reproductible
en utilisant du pure **numpy**.

La fonction **gradient** renvoie le gradient d’une grille non structurée
- un concept qui n’existe pas dans **numpy**.

Cependant, ici, nous conservons la facilité d’utilisation de **numpy**.

Ce module a été conçu pour simplifier l’accès aux ensembles de données
et tableaux VTK à partir de Python en fournissant une interface de style
**numpy**.

Le module **dataset_adapter** permet de convertir un objet de dataset
VTK existant en dataset_adapter.VTKObjectWrapper. Voyons comment
procéder en examinant ce code source

.. code:: python

   >>> import vtk
   >>> from vtk.numpy_interface import dataset_adapter as dsa

   >>> s = vtk.vtkSphereSource()

   >>> e = vtk.vtkElevationFilter()
   >>> e.SetInputConnection(s.GetOutputPort())
   >>> e.Update()

   >>> sphere = dsa.WrapDataObject(e.GetOutput())

   >>> print sphere
   <vtk.numpy_interface.dataset_adapter.PolyData object at 0x1101fbb50>
   >>> print isinstance(sphere, dsa.VTKObjectWrapper)
   True

Ici, nous créons une instance de la classe dataset_adapter.PolyData, qui
fait référence à la sortie du filtre vtkElevationFilter. Nous pouvons
accéder à l’objet VTK sous-jacent à l’aide du membre VTKObject

.. code:: python

   >>> print type(sphere.VTKObject)
   <type 'vtkobject'>

Notez que la fonction WrapDataObject() renverra une classe wrapper
appropriée pour toutes les sous-classes vtkDataSet, vtkTable et toutes
les sous-classes vtkCompositeData. Les autres sous-classes de
vtkDataObject ne sont actuellement pas prises en charge comme
vtkHyperTreeGrid.

VTKObjectWrapper transmet les méthodes VTK à son VTKObject afin que
l’API VTK soit directement accessible comme suit

.. code:: python

   >>> print sphere.GetNumberOfCells()
   96L

Cependant, VTKObjectWrappers ne peut pas être transmis directement aux
méthodes VTK en tant qu’argument

.. code:: python

   >>> s = vtk.vtkShrinkPolyData()
   >>> s.SetInputData(sphere)
   TypeError: SetInputData argument 1: method requires a VTK object
   >>> s.SetInputData(sphere.VTKObject)

Dataset Attributes
==================

Nous avons un wrapper pour les objets de données VTK qui se comporte
partiellement comme un objet de données VTK. Cela devient un peu plus
intéressant lorsque nous commençons à chercher comment accéder aux
champs (tableaux) contenus dans cet ensemble de données.

.. code:: python

   >>> sphere.PointData
   <vtk.numpy_interface.dataset_adapter.DataSetAttributes at 0x110f5b750>

   >>> sphere.PointData.keys()
   ['Normals', 'Elevation']

   >>> sphere.CellData.keys()
   []

   >>> sphere.PointData['Elevation']
   VTKArray([ 0.5       ,  0.        ,  0.45048442,  0.3117449 ,  0.11126047,
           0.        ,  0.        ,  0.        ,  0.45048442,  0.3117449 ,
           0.11126047,  0.        ,  0.        ,  0.        ,  0.45048442,
           ...,
           0.11126047,  0.        ,  0.        ,  0.        ,  0.45048442,
           0.3117449 ,  0.11126047,  0.        ,  0.        ,  0.        ], dtype=float32)

   >>> elevation = sphere.PointData['Elevation']

   >>> elevation[:5]
   VTKArray([0.5, 0., 0.45048442, 0.3117449, 0.11126047], dtype=float32)

Notez que cela fonctionne également avec les ensembles de données
composites

.. code:: python

   >>> mb = vtk.vtkMultiBlockDataSet()
   >>> mb.SetNumberOfBlocks(2)
   >>> mb.SetBlock(0, sphere.VTKObject)
   >>> mb.SetBlock(1, sphere.VTKObject)
   >>> mbw = dsa.WrapDataObject(mb)
   >>> mbw.PointData
   <vtk.numpy_interface.dataset_adapter.CompositeDataSetAttributes instance at 0x11109f758>

   >>> mbw.PointData.keys()
   ['Normals', 'Elevation']

   >>> mbw.PointData['Elevation']
   <vtk.numpy_interface.dataset_adapter.VTKCompositeDataArray at 0x1110a32d0>

Array API
=========

Il est possible d’accéder à PointData, CellData, FieldData, Points
(sous-classes de vtkPointSet uniquement), Polygones (vtkPolyData
uniquement) de cette manière. KitWare assure qu’ils continueront à
ajouter des accesseurs à plus de types de tableaux via cette API.

Enfin, nous arrivons à quelque chose de plus intéressant: travailler
avec des tableaux, des ensembles de données et des algorithmes. C’est là
que la numpy_interface brille et facilite considérablement certaines
tâches d’analyse de données. Commençons par un exemple simple.

.. code:: python

   >>> from vtk.numpy_interface import dataset_adapter as dsa
   >>> from vtk.numpy_interface import algorithms as algs
   >>> import vtk

   >>> w = vtk.vtkRTAnalyticSource()
   >>> w.Update()
   >>> image = dsa.WrapDataObject(w.GetOutput())
   >>> rtdata = image.PointData['RTData']

   >>> tets = vtk.vtkDataSetTriangleFilter()
   >>> tets.SetInputConnection(w.GetOutputPort())
   >>> tets.Update()
   >>> ugrid = dsa.WrapDataObject(tets.GetOutput())
   >>> rtdata2 = ugrid.PointData['RTData']

Ici, nous avons créé deux ensembles de données: une image de données
(vtkImageData) et une grille non structurée (vtkUnstructuredGrid). Ils
représentent essentiellement les mêmes données mais la grille non
structurée est créée en tétraédisant les données d’image. Nous nous
attendons donc à ce que la grille non structurée ait les mêmes points
mais plus de cellules (tétraèdres).

Les objets de tableau Numpy_interface se comportent de manière très
similaire aux tableaux numpy. En fait, les tableaux des sous-classes
vtkDataSet sont des instances de VTKArray, qui est une sous-classe de
numpy.ndarray. Les tableaux de vtkCompositeDataSet et les sous-classes
ne sont pas des tableaux numpy mais se comportent de manière très
similaire. Commençons par les bases. Tous les travaux suivants comme
prévu.

.. code:: python

   >>> rtdata[0]
   60.763466

   >>> rtdata[-1]
   57.113735

   >>> rtdata[0:10:3]
   VTKArray([  60.76346588,   95.53707886,   94.97672272,  108.49817657], dtype=float32)

   >>> rtdata + 1
   VTKArray([ 61.
   76346588,  86.87795258,  73.80931091, ...,  68.51051331,
           44.34006882,  58.1137352 ], dtype=float32)

   >>> rtdata < 70
   VTKArray([ True , False, False, ...,  True,  True,  True], dtype=bool)

   >>> # We will cover algorithms later. This is to generate a vector field.
   >>> avector = algs.gradient(rtdata)

   >>> # To demonstrate that avector is really a vector
   >>> algs.shape(rtdata)
   (9261,)

   >>> algs.shape(avector)
   (9261, 3)

   >>> avector[:, 0]
   VTKArray([ 25.69367027,   6.59600449,   5.38400745, ...,  -6.58120966,
           -5.77147198,  13.19447994])

Quelques points à noter dans cet exemple: - Les tableaux à un seul
composant ont toujours la forme suivante: (ntuples,) et non (ntuples, 1)
- Les tableaux de composants multiples ont la forme suivante: (ntuples,
ncomponents) - Les tableaux Tensor ont la forme suivante: (ntuples, 3,
3) - Ce qui précède vaut même pour les images et autres données
structurées. Tous les tableaux ont 1 dimension (1 tableaux de
composants), 2 dimensions (tableaux à plusieurs composants) ou 3
dimensions (tableaux de tenseurs).

De plus, il est possible d’utiliser des tableaux booléens pour indexer
des tableaux. Donc, ce qui suit fonctionne très bien:

.. code:: python

   >>> rtdata[rtdata < 70]
   VTKArray([ 60.76346588,  66.75043488,  69.19681549,  50.62128448,
           64.8801651 ,  57.72655106,  49.75050354,  65.05570221,
           57.38450241,  69.51113129,  64.24596405,  67.54656982,
           ...,
           61.18143463,  66.61872864,  55.39360428,  67.51051331,
           43.34006882,  57.1137352 ], dtype=float32)

   >>> avector[avector[:,0] > 10]
   VTKArray([[ 25.69367027,   9.01253319,   7.51076698],
          [ 13.1944809 ,   9.01253128,   7.51076508],
          [ 25.98717642,  -4.49800825,   7.80427408],
          ...,
          [ 12.9009738 , -16.86548471,  -7.80427504],
          [ 25.69366837,  -3.48665428,  -7.51076889],
          [ 13.19447994,  -3.48665524,  -7.51076794]])

Algorithmes
===========

On peut faire beaucoup en utilisant simplement l’API array. Cependant,
les choses deviennent beaucoup plus intéressantes lorsque nous
commençons à utiliser le module numpy_interface.algorithms. Pour une
liste complète des algorithmes, utilisez help(algs). Voici quelques
exemples explicites:

.. code:: python

   >>> algs.sin(rtdata)
   VTKArray([-0.87873501, -0.86987603, -0.52497   , ..., -0.99943125,
          -0.59898132,  0.53547275], dtype=float32)

   >>> algs.min(rtdata)
   VTKArray(37.35310363769531)

   >>> algs.max(avector)
   VTKArray(34.781060218811035)

   >>> algs.max(avector, axis=0)
   VTKArray([ 34.78106022,  29.01940918,  18.34743023])

   >>> algs.max(avector, axis=1)
   VTKArray([ 25.69367027,   9.30603981,   9.88350773, ...,  -4.35762835,
           -3.78016186,  13.19447994])

Si vous n’avez jamais utilisé l’argument axe, c’est assez simple.
Lorsque vous ne transmettez pas une valeur d’axe, la fonction est
appliquée à toutes les valeurs d’un tableau sans tenir compte de la
dimensionnalité. Lorsque axis = 0, la fonction sera appliquée à chaque
composant du tableau indépendamment. Lorsque axis = 1, la fonction sera
appliquée à chaque tuple indépendamment. Expérimentez si ce n’est pas
clair pour vous. Les fonctions qui fonctionnent de cette manière
incluent sum, min, max, std et var.

Une autre fonction intéressante et utile est où qui renvoie les indices
d’un tableau où une condition particulière se produit.

.. code:: python

   >>> algs.where(rtdata < 40)
   (array([ 420, 9240]),)

Pour les vecteurs, cela retournera également l’index du composant si un
axe n’est pas défini.

.. code:: python

   >>> algs.where(avector < -29.7)
   (VTKArray([4357, 4797, 4798, 4799, 5239]), VTKArray([1, 1, 1, 1, 1]))

Jusqu’à présent, toutes les fonctions dont nous avons parlé sont
directement fournies par numpy. De nombreux ufuncs numpy sont inclus
dans le module algorithmes. Ils fonctionnent tous avec des tableaux
uniques et des tableaux de données composites (plus d’informations à ce
sujet sur un autre blog). Les algorithmes fournissent également
certaines fonctions qui se comportent quelque peu différemment de leurs
homologues numpy. Celles-ci incluent la cross (produit vectoriel), dot
(produit scalaire), inverse, determinant (déterminant), eigenvalue
(valeurs propres), eigenvector (vecteurs propres), etc. Toutes ces
fonctions sont appliquées à chaque tuple plutôt qu’à un tableau /
matrice entier. Par exemple:

.. code:: python

   >>> amatrix = algs.gradient(avector)
   >>> algs.determinant(amatrix)
   VTKArray([-1221.2732624 ,  -648.48272183,    -3.55133937, ...,    28.2577152 ,
           -629.28507693, -1205.81370163])

Notez que tout ce qui précède n’utilise que les informations par tuple
et ne repose pas sur le maillage. L’une des plus grandes forces de VTK
est que son modèle de données prend en charge une grande variété de
maillages et que ses algorithmes fonctionnent de manière générique sur
tous ces types de maillages. Le module algorithmes expose certaines de
ces fonctionnalités. D’autres fonctions peuvent être facilement
implémentées en exploitant les filtres VTK existants. J’ai utilisé le
gradient avant pour générer un vecteur et une matrice. Le voici encore

.. code:: python

   >>> avector = algs.gradient(rtdata)
   >>> amatrix = algs.gradient(avector)

Des fonctions comme celle-ci nécessitent l’accès à l’ensemble de données
contenant le tableau et le maillage associé. C’est l’une des raisons
pour lesquelles nous utilisons une sous-classe de ndarray dans
dataset_adapter:

.. code:: python

   >>> rtdata.DataSet
   <vtk.numpy_interface.dataset_adapter.DataSet at 0x11b61e9d0>

Chaque tableau pointe vers l’ensemble de données le contenant. Des
fonctions telles que le dégradé utilisent le maillage et le tableau
ensemble. Numpy fournit également une fonction de dégradé, dites-vous.
Qu’est-ce qui vous passionne? Bon ça:

.. code:: python

   >>> algs.gradient(rtdata2)
   VTKArray([[ 25.46767712,   8.78654003,   7.28477383],
          [  6.02292252,   8.99845123,   7.49668884],
          [  5.23528767,   9.80230141,   8.3005352 ],
          ...,
          [ -6.43249083,  -4.27642155,  -8.30053616],
          [ -5.19838905,  -3.47257614,  -7.49668884],
          [ 13.42047501,  -3.26066017,  -7.28477287]])
   >>> rtdata2.DataSet.GetClassName()
   'vtkUnstructuredGrid'

Le dégradé et les algorithmes qui nécessitent l’accès à un maillage
fonctionnent que ce maillage soit une grille uniforme, une grille
curviligne ou une grille non structurée grâce au modèle de données de
VTK. Jetez un œil aux différentes fonctions du module algorithmes pour
voir toutes les choses intéressantes qui peuvent être accomplies en
l’utilisant. J’écrirai à l’avenir des blogs qui montreront comment des
problèmes spécifiques peuvent être résolus à l’aide de ces modules.

MPI
===

Tout cela fonctionne avec des jeux de données composites et en parallèle
avec MPI de VTK - via les propres vtkMultiProcessController et `mpi4py
de VTK <https://blog.kitware.com/mpi4py-and-vtk/>`__.

A poursuivre…
https://blog.kitware.com/improved-vtk-numpy-integration-part-4/
