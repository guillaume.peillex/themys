todo : il est indispensable que la documentation Themys soit
indépendante d’internet tout référence extérieure doit être bannie

Il faudrait voir à ramener la documentation :
https://docs.paraview.org/en/latest/UsersGuide/index.html avec themys et
ensuite faire des références vers cette version embarquée au lieu de
faire des références externes. Ceci serait plus conforme à notre souhait
d’embarquer tout avec Themys (vision ThirdParty mais pour la
documentation).

vtkPythonCalculator
===================

Évalue une expression Python vtkPythonCalculator utilise Python pour
calculer une expression.

Ce filtre dépend fortement des modules numpy et paraview.vtk. Pour
utiliser les fonctions parallèles, mpi4py est également nécessaire.
L’expression est évaluée et la valeur scalaire ou le tableau numpy
résultant est ajouté à la sortie sous forme de tableau. Voir la
documentation de numpy et paraview.vtk pour la liste des fonctions
disponibles.

Ce filtre essaie de faciliter l’écriture d’expressions par l’utilisateur
en définissant certaines variables. Le filtre essaie d’affecter chaque
tableau à une variable du même nom. Si le nom du tableau n’est pas une
variable Python valide, il doit être accessible via un dictionnaire
appelé tableaux (c’est-à-dire les tableaux [‘nom_tableau’]). Les points
sont accessibles à l’aide de la variable points.

https://vtk.org/Wiki/Python_Calculator

https://www.paraview.org/Wiki/Python_calculator_and_programmable_filter

inputS est la bonne syntaxe
