A revoir au regard de la documentation dans Procedures pour
PythonAnnotation: - le nom des variables aurait changer time_value au
lieu de t_value ? - comment définir une expression sur plusieurs lignes
? utilisation d’un module externe ?

GUI Variable
============

t_index : indice du temps

t_steps : la liste des temps t_value : la valeur du temps soit
t_steps[t_index]

t_range : les valeurs extrêmes soit t_steps[0] et t_steps[len(t_steps)]

Operators Variable
==================

https://gitlab.kitware.com/vtk/vtk/-/blob/master/Wrapping/Python/vtkmodules/numpy_interface/algorithms.py

https://gitlab.kitware.com/vtk/vtk/-/blob/master/Wrapping/Python/vtkmodules/numpy_interface/internal_algorithms.py

Api de simplification
=====================

Ceci étant dit, il existe déjà une API remplissant peut-être tes
attentes, qui est orienté vers les traitements numpy.
https://blog.kitware.com/improved-vtk-numpy-integration/

Sans utiliser des modules externes, il possible d’utiliser le script
suivant : ds = dsa.WrapDataObject(self.GetInput())
print(ds.PointData[‘RTData’])

Quid input ?
============

Import de module
================

-  Sources -> Wavelet, Apply
-  Filters -> Programmable Filter
-  Python Path = “path/to/dir” containing modulecea.py
-  Script =

import modulecea as m ds = m.ds(self.GetInput()) print(ds.pd.RTData)
