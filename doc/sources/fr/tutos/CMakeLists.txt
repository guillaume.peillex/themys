add_subdirectory(animation)
add_subdirectory(scripting)
add_subdirectory(themys)

set(DOC_FILES_FR
    ${DOC_FILES_FR} ${CMAKE_CURRENT_LIST_DIR}/tutos.rst
    PARENT_SCOPE)
