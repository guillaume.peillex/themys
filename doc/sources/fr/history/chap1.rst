Documentation: `doc en <../../en/history/chap1.html>`__



Au siècle dernier
=================

En 1993, le projet **The Visualization Toolkit**, `VTK <vtk.org>`__, est initié chez General Electric R&D.

En 1998, la société `Kitware <www.kitware.com>`__ est fondée afin de mieux répondre au besoin pressant d’une
communauté mondiale de plus en plus importante autour de l'utilisation de `VTK <vtk.org>`__.

`VTK <vtk.org>`__ est aujourd'hui un logiciel open source permettant de manipuler et d'afficher des données
scientifiques. Il est livré avec de nombreux outils pour réaliser des rendus 2D comme 3D, une suite
d'intéracteurs 3D, une capacité de traçage 1D et 2D étendue et bien sûr plusieurs lecteurs de données
comme d'écrivains.

`VTK <vtk.org>`__ fait partie d'une collection d'outils pris en charge par la société
`Kitware <www.kitware.com>`__. Cette plate-forme est utilisée dnas le monde entier dans diverses
applications commerciales, ainsi que dans la recherche et le développement.
