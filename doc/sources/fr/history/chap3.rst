Documentation: `doc en <../../en/history/chap3.html>`__



Depuis 2019
===========

En 2019, la décision a été prise au `CEA-EA/DAM <www.cea.fr>`__ de figer les développements de **LOVE**
au profit d’une utilisation future plus proche de **ParaView**.

Cela commence dès 2020 par :

* l'enrichissement de **ParaView**/**VTK** à dessein du `CEA-EA/DAM <www.cea.fr>`__ en s'inspirant de
  l'existant dans **LOVE** comme :

  * faciliter le retaillage de fenêtre sur des cas à long temps de rendu ;

  * synthètiser la consommation mémoire des serveurs ;

  * utiliser une commande système afin d'ajuster le temps d'allocation restant ;

  * faciliter l'accès aux palettes de couleur ;

  * etc ;

* la réécriture de notre lecteur au format de données propriétaire **Hercule**,

* le prototypage d'une **ParaView’s branded application**, **Themys**.

Par ailleurs, le choix est fait dès le début que la plate-forme **Themys** soit développé sur le réseau ouvert
en open-source afin de faciliter les développements, les prestations et les collaborations.
La société `Kitware <www.kitware.com>`__, sollicitée à travers des prestations, est un acteur important
dans le développement de cette plate-forme.

En 2021, **Themys** est présenté aux utilisateurs du `CEA-EA/DAM <www.cea.fr>`__ comme étant l'outil qui
va remplacer **LOVE**. Nativement, il englobe toutes les fonctionnalités proposer dans **ParaView** dont
certaines ont été personnalisés.

En 2022, **Themys** atteint une maturité suffisante pour remplacer **LOVE** pour une bonne part de nos applications.

En se limitant volontairement à l'aspect personnalisation de l'interface, **Themys** limite très fortement
le coût du portage lié aux évolutions de **ParaView**.
Le passage de **LOVE** à **Themys** a permis de passer d'un coût de plusieurs mois/an à quelques semaines/an,
permettant ainsi aux équipes d'être plus disponible auprès des utilisateurs.
