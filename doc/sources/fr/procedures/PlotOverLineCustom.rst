Documentation: `doc en <../../en/procedures/PlotOverLineCustom.html>`__



Extraction personnalisée de points sur lignes
=============================================

Description
===========

A partir d’un maillage, il est possible d’extraire une information portée par un ensemble de poly-lignes (équivalent d'une droite brisée) qui traverseraient ce maillage. C'est ce que propose de faire le **Plot Over Line Custom** (la version non *custom* ne s'appliquant que pour une droite).

Le rendu de l’application d’un tel filtre produit un **MultiBlockDataSet** contenant autant de bloc que de poly-lignes passaient en entrée. L’affichage standard produit des courbes avec en abscisse la position linéaire sur la poly-lignes depuis son origine et en ordonnée la valeur pour un des champs de valeurs.

La position linéaire, exprimé par Themys par le champ de valeurs **arc_length**, correspond à la distance couverte en parcourant la poly-ligne depuis le début.

Plusieurs options sont proposées :
- **Sample Uniformly** (échantillonner uniformément) extrait des points répartis uniformément sur chacune des poly-lignes, leur nombre par poly-ligne est piloté par la propriété **Resolution** (résolution) ; la résolution du résultat dépend directement de la discrétisation en points choisie ;
- **Sample At Cell Boundaries** (échantillon aux limites des cellules) extrait des parties (segment) qui traversent chacune des cellules sur chacune des poly-lignes ; pour un champ de valeurs aux cellules, le résultat se présente sous la forme d'un escalier, un plateau correspondant à la valeur considérée comme constante au sein d'une cellule traversée ; le résultat exprime clairement la géométrie du maillage ;
- **Sample At Segment Centers** (échantillon dans les centres de segments) extrait les centres des parties (segment) qui traversent des cellules sur chacune des poly-lignes ; c'est équivalent à la précédente option mais en ne conservant que le centre de chacun des segments.

Les résultats sont triés suivant la position linéaire croissante par poly-ligne.

Création du filtre PlotOverLineCustom
=====================================

La procèdure pour construire une instance de filtre **Plot Over Line Custom** nécessite la définition de deux entrées qui sont le mailage géométrique et un multi-poly-lignes.

Pour créer ce filtre, il suffit de sélectionner le maillage sur lequel on veut l'appliquer puis de créer un **PlotOverLineCustom** (CTRL^Espace puis commencer à saisir le nom du filtre pour que la liste cible ce filtre, ENTER une fois choisie).

Une fenêtre de dialoge s’ouvre alors pour nous proposer de fixer la valeur de chacun des deux ports :
- le premier nommé **Input** est déjà renseigné, c’est le maillage qui était actif ;
- le second nommé **Source** (cliquer dessus pour changer la valeur de ce port) doit décrire un multi-poly-lignes ; cela nécessite de sélectionner un élément dans le **Pipeline Browser**.

|image0|

Crétion d'un maillage multi-poly-lignes
=======================================

Des facilités sont proposées pour construire automatiquement des multi-poly-lignes de type :
- paon ou hérisson : **SpheresLineSource** ;
- faisceau : **BoxLinesSource** ; ou
- balayette : **CylinderLineSource**.
dans la GUI de Themys.

Mais de façon générale, vous pouvez créer votre propre source multi-poly-lignes en procédant de la façon suivante.

Etape 1 : créer une poly-ligne
------------------------------

Pour créer une poly-ligne, vous devez créer un filtre de type **Poly Line Source**.

Les **Properties** du filtre nouvellement créé apparaissent avec déjà deux points d’enregistrés en (0,0,0) et (1,0,0). Ensuite :
- soit vous pouvez modifer et ajouter des valeurs dans le tableau à la main, mais de façon précise ;
- soit vous pouvez modifier de façon intéractive la poly-lignes. Bien sûr, à tout moment, vous pouvez passer d’un mode à l’autre.

Pour modifier intéractivement le premier point qui sera **le point de début de votre poly-ligne**, vous devez activer la fenêtre de rendu qui vous intéresse puis déplacer le curseur de la souris à l’endroit de votre choix pour finalement appuyez sur la touche 1.

Vous procédez de façon similaire pour positionner le second point qui sera le **point de fin de votre poly-ligne** en appuyant sur la touche 2.

**En vous plaçant sur une des lignes**, vous pouvez ajouter un point en faisant CTR^clic. Pour déplacer un point existant, il suffit de cliquer sur un point puis :
- soit de le déplacer à l’aide de la souris (bouton clic gauche maintenu) ;
- soit de positionner la souris puis d’appuyer sur CTRL^P.

Un SHIFT^CTR^clic suffit pour détruire un point de la poly-ligne.

La poly-ligne peut être complétement déplacée à la main si vous cliquez sur une des lignes en évitant soigneusemnt un des points.

TODO Pas disponible en raccourci Le clic sur le boutton + sur le dernier point propose un point nouveau qui est la symétrie de l’avant dernier point par le centre qui est le dernier point. Autrement dit, reporte le vecteur défini par les deux derniers points pour construire le suivant.

La poly-ligne ainsi construire est un **Polygonal Mesh** ne comprenant qu’une cellule de type poly-ligne.

Etape 2 : grouper puis merger ces poly-lines
--------------------------------------------

Pour cela, vous devez sélectionner les poly-lignes que vous avez définies.

Puis, vous appliquez le filtre **Group DataSets** qui va définir un **MultiBlockDataSet** où chaque bloc est une poly-ligne.

Puis, vous appliquer le filtre **Merge Blocks** qui va définir un nouveau maillage non structuré où chaque bloc qui est une poly-ligne devient une cellule de type poly-lignes de ce maillage.

Ce résultat peut servir de **Source** au **PlotOverLineCustom**.

Exemple
=======

Veuillez commencer par ouvrir la base **SainteHelens.dem**, puis APPLY dans **Properties**.
Le volcan Sainte Hélène s’affiche à plat et propose un champ de valeurs qui décrit une **Elevation** aux points (le symbole associè est un petit rond jaune).

Particularité de ce maillage, on constate que la troisième valeur, **Z**, de **Bounds**` couvre de 682 à 682, cela est visible cela dans l’onglet **Information**.
Afin de faciliter notre tâche, nous optons pour effectuer une translation de -682 en Z. Pour cela, nous appliquons
le filtre **Transform** en saisissant cette valeur dans le troisième champ de la ligne **Translate** puis APPLY. On constate alors que la valeur **Z** de **Bounds** a changé pour prendre la valeur 0.

A l’issue de cela, nous avons un maillage **Structured (Curvilinear) Grid** qui est pris en compte par le filtre **PlotOverLineCustom**.

Ensuite, on construit un, puis deux à autant de **Poly Line Source** souhaitées. Dans notre exemple, il faudra faire attention que les valeurs en **Z** des points de ces poly-lignes soient nulles.

Puis, pour finir, il faut sélectionner toutes les **Poly Line Source** afin d’appliquer un premier filtre de **Group DataSets** puis de **Merge Blocks**.

Il reste plus qu’à définir un filtre **Plot Over Line Custom** en ayant auparavant cliqué sur le maillage produit par l’opération de **Transform**.
Une fenêtre de dialogue s’ouvre pour gérer les inputs :
- on constate que **Input** est bien sur le maillage produit par **Transform** ;
- on clique sur **Source** et ensuite on sélectionne le filtre de **Merge Blocks** ;

Ne pas oublier de cliquer sur les différents blocks afin d’avoir les résultats correspondant à chaque poly-lignes, puis **APPLY**.

**arc_length** est par défaut utiliser. Cette valeur est utilisée en abscisse pour chaque poly-ligne. Elle correspond à la distance linèaire sur la poly-lignes depuis le premier point.

Astuce
======

Lors de l’application du filtre **Transform**, il peut être parfois judicieux de normaliser le maillage afin d’avoir plus de facilité à saisir les valeurs des points des poly-lignes.

Nous vous proposons ainsi de déplacer le maillage afin d’avoir une origine à (0;0;0) puis d’appliquer un facteur d’échelle correspondant au plus petit côté.
Dans cet exemple, les coordonnées des points se retrouvent ainsi dans les intervalles suivants :
- pour X dans [0.; 1.] ;
- pour Y dans [0.; 1.43]

Voici deux propositions pour faire cette transformation :
- la première est humainement compréhensible mais nécessite d’appliquer deux fois un filtre de **Transform** ;
- la seconde est plus efficace car se résume à un filtre **Transform** mais nécessite un calcul intermédiaire.

Proposition 1 : **Transform Translate** puis **Transform Scale**
----------------------------------------------------------------

Cette proposition est plus humainement compréhensible.
On détermine les paramètres de la translation, puis de la réduction.

Dans notre exemple, la normalisation passerait par un filtre **Transform** de translation :
- Translate -557945; -5.1079915e6; -682
puis un second filtre **Transform** de rotation :
- Scale 0.000102249; 0.000102249; 1

Proposition 2 : **Trasnform Translate/Scale**
---------------------------------------------

Cette proposition est moins compréhensible car elle nécessite l’application d’un calcul parce que la translation est faite après la réduction.

Comme le filtre **Transform** applique d’abord le **Scale** avant la **Translation**, cela donne les paramètres suivants dans un unique filtre :
- Translate : Le calcul suivant doit être fait -557945/0.000102249; -5.1079915e6/0.000102249; -682 ce qui donne les
valeurs -57.0493; -522.287; 0
- Scale 0.000102249; 0.000102249; 1

Par contre, il est très fortement conseillé d’utiliser cette façon de faire. En effet, chaque application du filtre **Transform** entraîne nécessairement une duplication complète des coordonnées du maillage.

Bug
===

Actuellement, dans le cas d’une extraction d’une valeur aux points, nous conseillons d’appliquer, avant ou après le filtre **Transform**, un filtre **Point Data to Cell Data** pour projeter les valeurs des points aux cellules. Si l’on a opté pour placer ce filtre après **Transform**, ne pas alors oublier de modifier **Input** du filtre **Plot Over Line Custom**. Le rendu sera correct si l’on opte pour l’option **Sample At Cell Boundaries**.

Calcul d’une intégrale linéaire
===============================

TODO Utilisation du plugin filter

TODO Voir comment procéder pour que les poly-lines restent visibles sur
le maillage (tube ?). TODO Idée : définir une view proposant les boutons
de gestion de MBDS. TODO Transform faire évoluer avant de décrire dans
l’ordre les transformations unitaires que l’on souhaite appliquer

.. |image0| image:: ../../img/procedures/plotoverlinecustom_ports.png
