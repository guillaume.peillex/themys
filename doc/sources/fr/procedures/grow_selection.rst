

Etendre une sélection
=====================

Lorsqu'une sélection de cellules est définie, il est possible d'en étendre son contenu en sélectionnant
les cellules voisines.

Via l'interface graphique
-------------------------

|image0|

Cette barre de menu proposée dans **Render View** (fenêtre de rendu) permet :

* de déclencher le mode de sélection des cellules en cliquant sur le bouton (1), il vous reste ensuite à :

    * cliquer sur une cellule pour l'inclure dans la sélection ; les cellules ainsi sélectionnées ont leurs
      arêtes surlignés en couleur fushia ;

    * SHIFT+cliquer sur une cellule sélectionné pour la retirer de la sélection ;

* d'étendre la sélection sur les cellules voisines en cliquant sur le bouton (2) ;

* de revenir en arrière sur cette extension de sélection en cliquant sur le bouton (3) ; au bout d'un certain
  temps, ce bouton n'est plus actif car nous sommes revenus à la sélection initiale ;

* de vider la sélection en cliquant sur le bouton (4).

Option du grossissement de la sélection
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Les réglages de **Themys** permet de modifier le choix du mécanisme du grossissement de la sélection.

Pour accéder à ces options de réglages, l'utilisateur doit aller dans le menu *Edit > Settings*
et choisir l'onglet *Render View*, ensuite, afin d'accéder aux options avancées, cliquer sur le
bouton *roue dentée* à droite de la zone de saisie, et terminer en tapant *grow* dans cette même zone
de saisie.

|image1|

Lors de l'extension de la sélection, l'utilisateur ainsi décider :

* de conserver (*unchecked*) ou non (*checked*), les cellules souches de la sélection initiale à
  travers l'option **Remove seed on grow selection** ;

* de conserver (*unchecked*) ou non (*checked*), les cellules intermédiaires entre les cellules souches
  de la sélection initiale et celles qui ont été rajoutées par le dernier grossissement à travers
  l'option **Remove intermediate layers on grow selection**.

Ne pas oublier sur **Apply** ou **OK** afin que cela soit pris en compte sur une nouvelle sélection.

.. |image0| image:: ../../img/procedures/grow_selection.png
.. |image1| image:: ../../img/procedures/grow_selection_settings.png
