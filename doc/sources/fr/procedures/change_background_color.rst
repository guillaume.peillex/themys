Documentation: `doc en <../../en/procedures/change_background_color.html>`__



Changer la couleur de fond
==========================

Trouver les propriétés pour la couleur de fond
----------------------------------------------

Sélectionner un *élément* dans le navigateur du pipeline (**Pipeline Browser**) en cliquant dessus
pour la rendre active.

La terminologie utilisée par ParaView pour décrire cet objet actif du pipeline est *source*.
En effet, la méthode Python d'interrogation permettant de récupérer cet élément est *GetActiveSource()*.

Aller dans l'onglet **Properties** (propriétés).

1. Mode de recherche manuel
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Passer en mode avancé en cliquant sur le bouton représentant une petite roue à la fin de la ligne d'édition
(**Search for properties by name**).

Se déplacer vers le bas à la recherche de la propriété nommée **Background** (l'arrière plan).

2. Mode de recherche automatique
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Entrer dans la ligne d'édition de recherche d'une propriété par son nom (**Search for properties by name**)
les premières lettres de **Background**, e.g. Ba.

Apparaît alors les propriétés commençant par ces premières lettres.

.. note::
   Ne pas oublier de supprimer le contenu de la ligne d'édition (**Search for properties by name**)
   sinon vous ne verrez plus les autres propriétés.

Changer la propriété couleur du fond
------------------------------------

Differents modes sont proposés.

Le plus couramment utilisé est la couleur uniforme simple (**Single Color**).

Le choix de la couleur se fait en cliquant sur le bouton **Color** juste en-dessous afin de
retrouver la couleur actuellement sélectionnée dans une représentation des couleurs sous forme de disque.

Il vous suffit ensuite de choisir une autre couleur et de le valider.
