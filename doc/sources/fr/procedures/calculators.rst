Documentation: `doc en <../../en/procedures/calculators.html>`__



Calculatrices
=============

Description
-----------

La création de nouveaux champs de valeurs par un calcul faisant intervenir
des champs existants est chose courante.

C'est le cas de la création d'un
champ de valeurs représentant une *énergie cinétique* qui est réalisée en
effectuant le produit de deux champs de valeurs dont l'un représente une
*masse* et l'autre une *vitesse*.

Dans ce contexte de création, il est important de préciser que la valeur à
une cellule (resp. noeud) d'un nouveau champ est obtenue à partir des valeurs
de cette même cellule (resp. noeud) attributées à d'autres champs existants.

Les opérations incluant des valeurs aux cellules voisines ne sont donc pas
permises par ces calculatrices. C'est tout particulièrement le cas de la
création d'un champ de valeurs décrivant un gradient (resp. divergence,
vorticité, critère Q) qui n'est pas possible de réaliser avec une calculatrice
mais qui est néanmoins disponible à travers un filtre dédié (**Gradient**).

Via l'interface graphique
-------------------------

1. Calculator
~~~~~~~~~~~~~

L'application du filtre **Calculator** nécessite de sélectionner une des
instances d'objets/sources du navigateur du pipeline (**Pipeline Browser**)
en cliquant dessus avant de créer une instance de ce filtre **Calculator**.
Cette création peut se faire en cliquant sur un bouton ou en passant par
le menu déroulant **Filter**.

|image0|

.. note::
   Cette création d'instance de filtre peut aussi se faire en tapant **CTRL+ESP**
   (équivalent à passer par le menu déroulant *Filters > Search*)
   ce qui aura pour effet d'ouvrir une fenêtre contextuelle dans laquelle
   vous pouvez commencer à taper le nom du filtre recherché (ici, **calculator**).
   Ici, il se trouve que les deux premiers caractères du nom de ce filtre
   sont discriminants et vont mettre en surbrillance ce nom dans la liste
   des filtres disponibles. Il suffit donc de saisir **ca+ENTER**.

Ce filtre crée un nouveau champ de valeurs basé sur une expression
mathématique qui peut éventuellement inclure des champs de valeurs existants.
Visuellement, cette calculatrice a l'apparence d'une calculatrice scientifique.
Les fonctionnalités de base sont disponibles via des boutons, bien que
l'expression puisse être saisie manuellement. Les champs de valeurs
existants peuvent être facilement ajoutés avec les **Scalars** et
**Vectors**.

.. note::
   Les **Vectors** contiennent des composants, quand ils
   décrivent un champ spatial on parle de coordonnées, dont on peut
   extraire séparément un composant, la valeur de la magnitude
   (qui est la racine carrée de la somme de chaque compostant au carré).

Le choix du type de géométrie sur lequel s'appuiera le nouveau champ de
valeur peut être choisi à travers le menu déroulant **Attribute Type**.

Dans **Properties** (propriétés), après avoir cliqué sur **Apply** (appliquer),
le nouveau champ de valeurs est alors disponible avec le nom que l'utilisateur
a saisi. Ce champ est considéré comme les autres et peut donc être manipulé
comme d'habitude. L'image ci-dessous illustre le calcul de la norme des
points de consigne des données.

|image1|

.. note::
   Multiplier un champ vectoriel par **iHat** (resp. **jHat** et **kHat**)
   permet d'extraire la première (reps. deuxième et troisième) composante
   scalaire de ce vecteur.

   L'opérateur **mag(vel)** permet de calculer la magnitude du vecteur,
   sa longueur, sa norme euclidienne ou norme 2 du vecteur **vel**.
   Le calcul ainsi pratiqué est équivalent à l'expression mathématique
   suivante  **sqrt((vel*iHat)^2+(vel*jHat)^2+(vel*kHat)^2)**.

.. note::
   La calculatrice (**Calculator**) intégre aussi le
   gestionnaire d'expression (**Expression Manager**).

2. Python Calculator
~~~~~~~~~~~~~~~~~~~~

La calculatrice Python est très similaire à la calculatrice standard
(**Calculator**) en ce sens qu'elle traite un ou plusieurs champs de
valeurs disponibles en fonction d'une expression fournie par
l'utilisateur afin de produire un nouveau champ de valeurs.
Cependant, elle utilise le langage dynamique Python et NumPy
pour effectuer le calcul.
Des opérations prédéfinies sont proposées ce qui offre par
conséquent des capacités d'expression plus expressives.

Une composante d'un champ vectoriel est ainsi accessible avec des
crochets. Pour plus de détails sur les fonctionnalités disponibles,
voir `la page de documentation <https://docs.paraview.org/en/latest/UsersGuide/filteringData.html#python-calculator>`__.

L'application du filtre **Python Calculator** nécessite de sélectionner une des
instances d'objets/sources du navigateur du pipeline (**Pipeline Browser**)
en cliquant dessus avant de créer une instance de ce filtre **Python Calculator**.
Cette création peut se faire en cliquant sur un bouton.

`` panneau par clic sur cela. Cliquez ensuite sur le filtre **Python Calculator**
   (calculatrice Python) dans la barre d'outils des filtres, *Filters > Search*
   comme indiqué ci-dessous.

|image2|

.. note::
   Cette création d'instance de filtre peut aussi se faire en tapant **CTRL+ESP**
   ce qui aura pour effet d'ouvrir une fenêtre contextuelle dans laquelle
   vous pouvez commencer à taper le nom du filtre recherché (ici, **python calculator**).
   Au fur et à mesure de la saisie, le premier nom de filtre commençant
   par la saisie en cours est automatiquement mis en *surbrillance*.
   Vous pouvez prendre la main sur la recherche textuelle afin de vous
   déplacer dans la liste avec les touches de déplacement
   **UP** (haut) et **DOWN** (bas).
   La validation se fait en tapant sur **ENTER** ce qui aura pour effet de
   créer un filtre du nom qui était en surbrillance.
   Si le nom du filtre souhaité est grisé, non accessible, c'est qu'il
   ne peut être appliqué sur l'output de la source actuellement
   active dans le navigateur du pipeline (**Pipeline Browser**).
   Vous pouvez interrompre la recherche en appuyant sur **ESC**.

|image3|

Afin d'indiquer sur quelle géométrie (cellule ou point/noeud) s'appliquera
ce nouveau champ, vous devez le sélectionner dans la liste déroulante
**Array Association** (association du tableau).
Ensuite, il ne reste plus qu'à l'utilisateur de saisir l'expression en Python
dans la zone de saisie prévue à cet effet **Expression**.
Lorsque l'expression est terminée, cliquez sur **Apply** (appliquer) pour
déclencher le calcul et la création de ce nouveau champ de valeurs qui sera
automatiquement rajouté à celles qui sont déjà existantes.
Si une erreur vient à se produire durant le calcul, le champ de valeurs
n'est tout simplement pas créé.

|image4|

.. note::
   Dans le calculateur Python, il n'y a pas de **iHat** (resp. **jHat** et
   **kHat**), l'accès à une composante du vecteur **vel** se fait tout
   simplement par application de l'opérateur crochet comme **vel[1]**
   pour accéder à la seconde composante qui parfois est considérée
   comme la composante spatiale Y.

   L'expression **mag(vel)** permet de calculer la magnitude d'un vecteur,
   sa longueur, sa norme euclidienne ou norme 2 du vecteur **vel**.
   Elle est équivalente aux expressions mathématiques :

   # **sqrt(dot(vel, vel))**, avec **dot** la fonction pour effectuer le
     produit scalaire entre deux vecteurs,

   # **sqrt((vel[0])^2+(vel[1]])^2+(vel[2]])^2)**.

.. note::
   La calculatrice Python (**Python Calculator**) intégre aussi le
   gestionnaire d'expression (**Expression Manager**).

a. Les basiques
^^^^^^^^^^^^^^^

Après avoir créé une instance de source **Sphere** et dessus
une instance de fitlre **Python Calculator**, une première expression
utilisée peut être : 5.

Cela permet de créer un champ **result** sur les points où a été
attribué à chaque point la valeur 5. Automatiquement, un tableau
constant est alors créé.

L'expression **Normals** permet d'attribuer à **result** les
valeurs de ce tableau.

Une autre expression possible est **sin(Normals)+5**.

Rappelons qu'il est important de bien choisir la géométrie
qui sera associée au nouveau champ de valeurs. La plupart
des fonctions décrites par la suite s'appliquent individuellement
à toutes les valeurs d'un point ou d'une cellule et produisent
un champ de valeurs de même dimension que ceux en entrée.
Cependant, certains d'entre eux, tels que min() et max(),
produisent des valeurs globales uniques, non dimensionnées
au nombre de point ou de cellule.

Dans le filtre programmable, toutes les fonctions de
***vtk.numpy_interface.algorithms*** sont importées
avant l'exécution du script.
Par conséquent, certaines fonctions intégrées,
telles que min et max, sont encombrées par cette importation.
Pour utiliser les fonctions intégrées, importez le module
d'importation **__builtin__** et accédez à ces
fonctions avec, par exemple, **__builtin__.min** et **__builtin__.max**.

b. Les fonctions
^^^^^^^^^^^^^^^^

L'expression Python supporte l'utilisation des fonctions ou
opérateurs suivants et ceci peu importe le type géométrique
(**Cell** ou **Point**) sur lequel cela se fera.

.. warning::
   Il est très important de garder à l'esprit que l'application
   d'une fonction se fait en ne considérant que les valeurs des
   champs présents dans une cellule (resp. point).

+-------------+--------------------------------+
|     Nom     |            Opérandes           |
+             +--------------------------+-----+
|             |     Types supportées     | Nbr |
+=============+========+========+========+=====+
| abs         | scalar | vector | tensor |  1  |
+-------------+--------+--------+--------+-----+
| cross       |        | vector |        |  2  |
+-------------+--------+--------+--------+-----+
| curl        |        | vector |        |  1  |
+-------------+--------+--------+--------+-----+
| det         |        |        | tensor |  1  |
+-------------+--------+--------+--------+-----+
| determinant |        |        | tensor |  1  |
+-------------+--------+--------+--------+-----+
| dot         | scalar | vector |        |  2  |
+-------------+--------+--------+--------+-----+
| eigenvalue  |        |        | tensor |  1  |
+-------------+--------+--------+--------+-----+
| eigenvector |        |        | tensor |  1  |
+-------------+--------+--------+--------+-----+
| global_mean | scalar | vector | tensor |  1  |
+-------------+--------+--------+--------+-----+
| global_max  | scalar | vector | tensor |  1  |
+-------------+--------+--------+--------+-----+
| global_min  | scalar | vector | tensor |  1  |
+-------------+--------+--------+--------+-----+
| gradient    | scalar | vector |        |  1  |
+-------------+--------+--------+--------+-----+
| inverse     |        |        | tensor |  1  |
+-------------+--------+--------+--------+-----+
| laplacian   | scalar |        |        |  1  |
+-------------+--------+--------+--------+-----+
| ln          | scalar | vector | tensor |  1  |
+-------------+--------+--------+--------+-----+
| log(==ln)   | scalar | vector | tensor |  1  |
+-------------+--------+--------+--------+-----+
| log10       | scalar | vector | tensor |  1  |
+-------------+--------+--------+--------+-----+
| max         | scalar | vector | tensor |  1  |
+-------------+--------+--------+--------+-----+
| min         | scalar | vector | tensor |  1  |
+-------------+--------+--------+--------+-----+
| mean        | scalar | vector | tensor |  1  |
+-------------+--------+--------+--------+-----+
| mag         | scalar | vector |        |  1  |
+-------------+--------+--------+--------+-----+
| norm        | scalar | vector |        |  1  |
+-------------+--------+--------+--------+-----+
| strain      |        | vector |        |  1  |
+-------------+--------+--------+--------+-----+
| trace       |        |        | tensor |  1  |
+-------------+--------+--------+--------+-----+
| vorticity   |        | vector |        |  1  |
+-------------+--------+--------+--------+-----+

c. Les fonctions géométriques
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

L'expression Python supporte aussi l'utilisation de
fonctions qui ont été ajoutées dans ParaView mais
qui dépend du type de cellule associé à un maillage
non structuré.

+----------------+-----+------+-----+-----+-----------+
| Name           | Tri | Quad | Tet | Hex | Géometrie |
+================+=====+======+=====+=====+===========+
| area           | X   |  X   |     |     | cellule   |
+----------------+-----+------+-----+-----+-----------+
| aspect         | X   |  X   | X   |     | cellule   |
+----------------+-----+------+-----+-----+-----------+
| aspect_gamma   |     |      | X   |     | cellule   |
+----------------+-----+------+-----+-----+-----------+
| diagonal       |     |      |     | X   | cellule   |
+----------------+-----+------+-----+-----+-----------+
| jacobian       |     |  X   | X   | X   | cellule   |
+----------------+-----+------+-----+-----+-----------+
| max_angle      | X   |  X   |     |     | cellule   |
+----------------+-----+------+-----+-----+-----------+
| min_angle      | X   |  X   |     |     | cellule   |
+----------------+-----+------+-----+-----+-----------+
| shear          |     |  X   |     |     | cellule   |
+----------------+-----+------+-----+-----+-----------+
| skew           |     |  X   |     | X   | cellule   |
+----------------+-----+------+-----+-----+-----------+
| surface_normal | X   |      |     |     | cellule   |
+----------------+-----+------+-----+-----+-----------+
| volume         |     |      | X   | X   | cellule   |
+----------------+-----+------+-----+-----+-----------+
| vertex_normal  | X   |      |     |     | point     |
+----------------+-----+------+-----+-----+-----------+

.. note::
    Ces fonctions rentrent dans l'offre de différentes mesures
    pour rentrant dans l'évaluation de la qualité d'un maillage,
    voir VTK/Wrapping/Python/vtkmodules/numpy_interface qui
    propose en plus *condition* as the maximum Frabenius condition,
    mais ne propose pas *surface_normal* et  *vertex_normal*
    qui se base sur le filtre **vtkPolyDataNormals** avec
    des options différentes (pour les options
    *ComputeCellNormal* et *ComputePointNormal*, respectivement
    On/Off et Off/On).

============== === ==== === === ================
Name           Tri Quad Tet Hex Data association
============== === ==== === === ================
area           X   X            cell
aspect         X   X    X       cell
aspect_gamma            X       cell
diagonal                    X   cell
jacobian           X    X   X   cell
max_angle      X   X            cell
min_angle      X   X            cell
shear              X            cell
skew               X        X   cell
surface_normal X                cell
volume                  X   X   cell
vertex_normal  X                point
============== === ==== === === ================

d. Exemples
^^^^^^^^^^^

Calcul de l'aire d'une cellule
''''''''''''''''''''''''''''''

``area(inputs[0])`` produit un nouveau champ de valeurs sous CellData.

|image5|

Compute vector length for each point
''''''''''''''''''''''''''''''''''''

``sqrt(dot(BrownianVectors,BrownianVectors))`` or equivalently
``mag(BrownianVectors)`` where ``BrownianVectors`` is an array of 3D
vectors that are associated with each point in the dataset. Array
association set to Point Data.

Third example
'''''''''''''

``max(abs(trace(inverse(gradient(Normals)))))`` Normals is an array of
3D vectors that are associated to each point in the dataset.

|image6|

The expression first computes for each component of the normal vector
its gradient. It generates a tensor and computes the reverse of the
generated matrix. Then it sums everything on the diagonal of the reverse
matrix and computes the absolute value of the sum. Finally, it looks for
the maximum of all the absolute values.

**WARNING** **The numpy operators (section a) work on 1D arrays without
taking into account the topology of the mesh. In fact the calculations
requiring the neighborhood such as the gradient, exploit the neighboring
values ​​which are nearby (before or after) in memory.**

3. Gestionnaire d'expression
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Que ce soit pour la calculatrice (**Calculator**) ou la calculette Python
(**Python Calculator**) accessible par des boutons sous le champ de saisie
de l'expression, **Themys** propose un gestionnaire d'expression
(**Expression Manager**) afin de faciliter la gestion pérenne
d'expression en les stockant et en leur donnant
un accès rapide. Un nom est ainsi attribué à chaque expression ainsi
qu'un groupe, facilitement leur tri.

https://docs.paraview.org/en/latest/_images/FilteringExpressionManager-PropertyIntegration.png

Fig. # Boutons liés aux expressions dans le panel des propriétés (**Properties**)

La boîte de dialogue **Choose Expression**, également accessible à
partir du menu **Tools > Manage Expressions**, est une liste modifiable
et consultable des expressions stockées. **Themys** les suit via
les paramètres, mais ils peuvent également être exportés vers un
fichier JSON pour la sauvegarde et le partage entre les utilisateurs.

https://docs.paraview.org/en/latest/_images/FilteringExpressionManager-Dialog.png

Fig. # Boîte de dialogue du gestionnaire d'expression

Procedure using Python scripting
--------------------------------

.. _calculator-1_fr:

1. Calculator
~~~~~~~~~~~~~

.. code:: py

   # Find the source data set with its name
   # Replace 'Wavelet1' according to the desired data set
   mySource = FindSource('Wavelet1')

   # Create a new 'Calculator' filter
   calculator = Calculator(Input=mySource)

   # If needed, change array type to 'Point Data' or 'Cell Data'
   calculator.AttributeType = 'Point Data'

   # Define expression for output array
   calculator.Function = 'sqrt(coordsX^2+coordsY^2+coordsZ^2)'

   # Set output array name
   calculator.ResultArrayName = 'Norm'

   # Get active render view
   renderView = GetActiveViewOrCreate('RenderView')

   # Show output data
   calculatorDisplay = Show(calculator, renderView)

   # Hide original source data
   Hide(mySource, renderView)

   # Show color bar
   calculatorDisplay.SetScalarBarVisibility(renderView, True)

.. _python-calculator-1_fr:

2. Python Calculator
~~~~~~~~~~~~~~~~~~~~

.. code:: py

   # Find the source data set with its name
   # Replace 'Wavelet1' according to the desired data set
   mySource = FindSource('Wavelet1')

   # Create a new 'PythonCalculator' filter
   pyCalc = PythonCalculator(Input=mySource)

   # If needed, change array type to 'Point Data' or 'Cell Data'
   pyCalc.ArrayAssociation = 'Point Data'

   # Define expression for output array
   pyCalc.Expression = 'sin(RTData)'

   # Set output array name
   pyCalc.ArrayName = 'Result'

   # Get active render view
   renderView = GetActiveViewOrCreate('RenderView')

   # Show output data
   pyCalcDisplay = Show(pyCalc, renderView)

   # Hide original source data
   Hide(mySource, renderView)

   # Show color bar
   pyCalcDisplay.SetScalarBarVisibility(renderView, True)

.. |image0| image:: ../../img/procedures/10Calculator.png
.. |image1| image:: ../../img/procedures/10CalculatorUsage.png
.. |image2| image:: ../../img/procedures/10FiltersMenu.png
.. |image3| image:: ../../img/procedures/10FilterSearch.png
.. |image4| image:: ../../img/procedures/10PythonCalculatorUsage.png
.. |image5| image:: ../../img/procedures/PythonCalculator_1.png
.. |image6| image:: ../../img/procedures/PythonCalculator_2.png
