# This function mirror files in argument into the destination root directory
# reproducing the same relative path in the destination as the one in the origin
# root directory. It produces a target named "MirrorFiles_" suffixed with the
# label in argument.
#
# Parameters: - files_to_mirror: a list of absolute file paths to be mirrored -
# origin_root: the path to a directory holding all the files to mirror and that
# will be used to compute the relative path of each file to mirror -
# destination_root: the path to the directory where the files will be mirrored -
# target_label: identifier of the target that triggers the mirroring operation
function(mirror_files files_to_mirror origin_root destination_root target_label)
  set(copied_files "")

  foreach(filepath ${files_to_mirror})
    # filename is the name of the file to be mirrored
    get_filename_component(filename ${filepath} NAME)
    # relative_path is the relative path from the origin root directory to the
    # file that has to be mirrored and also the relative path from the
    # destination root directory to the mirrored file
    file(RELATIVE_PATH relative_path ${origin_root} ${filepath})
    # destination_path is the path to the mirrored file
    set(destination_path ${destination_root}/${relative_path})
    get_filename_component(destination_directory ${destination_path} DIRECTORY)

    add_custom_command(
      OUTPUT ${destination_path}
      COMMAND cmake -E make_directory ${destination_directory}
      COMMAND cmake -E copy_if_different ${filepath} ${destination_path}
      DEPENDS ${filepath}
      COMMENT "Copying file ${filename} into ${destination_path}")

    list(APPEND copied_files ${destination_path})
  endforeach()

  # Creates a target that will trigger the copy of the images
  add_custom_target(
    "MirrorFiles_${target_label}"
    DEPENDS ${copied_files}
    COMMENT "Mirroring files from ${origin_root} to ${destination_root}")
endfunction()
