Amélioration de Themys:



[Remarque] Il faut repenser la gestion des temps: meilleur accès au choix des temps ? possiblité de changer le pas de temps. Est-ce que il faut passer par le panel d'animation ? Possibilité de charger par intervalle (ne pas charger tous les temps) ?
----> regarder la doc de l'animation view

[Remarque] Intégrer l'assistant à themys: avoir un lien rapide vers le store/work/home... possiblement ouvrir un cas selon le nom du cas et sans toute l'arborescence.
Lors de la recherche de fichier, le moteur de navigation regroupe les noms de dossiers similaire (par exemple les dossiers f1 f2 f3 et f4 vont être regroupé dans "f..." et il faut cliquer sur la flèche à côté de "f..." pour faire apparaître tous les choix. => possiblité de choisir cette méthode ou tout lister, selon l'utilisateur ?
----> dans la partie a reverser

[Remarque] Il serait pratique de garder en mémoire la palette de couleur que l'on choisi afin que si on change de grandeur, la palette reste la même. (eg. choix d'une palette par defaut custom ?)
----> issue faite

[Remarque] Le choix de la palette de couleur se fait via le bouton "preset" et pour choisir il faut cliquer sur la pallette puis sur "apply" et "close". Il faudrait l'option de choix de la pallette avec la molette de la souris.
----> issue faite

[Remarque] Il faudrait pouvoir afficher la box des axes et qu'elle reste visible même quand je zoome.
----> dans la partie a reverser

[Remarque] Le bouton recentrer devrait centrer par rapport aux figures visibles (activées dans le multi-block inspector) et non par rapport aux grandeurs chargées.
----> issue faite

[Remarque] Rendre le script de lancement plus robuste: gestion des signaux ctrl+C, kill des alllocation ou bien informer l'utilisateurs avec le batchid...
----> voir plus tard en interne

[Question] Est-ce qu'il y a la possibilité d'afficher les maillages par composantes et non en globalité: "surface + edge" donne tous les maillages, on voudrait juste la délimitation des composantes (pas prioritaire) -> feature edge ?
----> utiliser des filtres qui ne font rien
[Question] Pour visualiser plusieurs grandeurs dans un même layout, y a-t-il d'autre solution que de faire des filtres qui ne font rien ?
----> attendre formation paraview

[Remarque] Le panel "properties" est un peu compliqué: il faut une doc et une intro à paraview
----> a faire en interne

[Remarque] Il faut une doc sur le panel "animation view"
----> a faire en interne

[Remarque] Le double scroll (scroll du panel properties et scroll des grandeurs) c'est pas ouf, mais y a-t-il mieux ?
----> si un utilisateur trouve mieux, on verra

[Remarque] Le bouton "API HIC usage" devrait porter un nom plus explicite, ou ne pas apparaitre si on laisse le HIc tout le temps
----> oui. Trouver un meilleur nom

[Remarque] Quand on ouvre une base, il faut mettre le readers CEA par défaut pour les bases hdep.
----> Test JB

[BUG] En parallèle, le passage themys/paraview plante, et à louverture de themys le plugin manager s'ouvre avec un "/!\" devant "themysSettings".
----> issue faite

[Remarque] Ajouter les icones rapide: Undo/Redo; et "ouvrir un fichier"
----> issue faite

[Remarque] Passer de themys à paraview (et inversement) compte comme une action qui peut être annulé (undo/redo): ça ne devrait pas.
----> issue faite

[Remarque] Refonte des menus déroulants:
	- Le menu "File" devrait contenir: open file, load state, save state, quit (et peut-être l'option de screenshot ?)

	- Le menu "Edit" devrait contenir: undo, redo, camera undo, camera redo, find data, reset session, settings

	- Le menu "View" devrait ouvrir un manager (une nouvelle fenêtre qui permettrait de choisir en une fois ce qu'on coche et décoche et voir l'IHM résultante avant de valider) -> garder tel quel

	- Le menu "Source" devrait contenir: themys et alphabetical (je ne saurais pas dire ce qu'il faut comme source mais laisser la possibilité d'avoir celle que l'utilisateur veut) -> pas poir l'instant

	- Le menu "Filter" pareil -> pareil

	- Le menu "Tools" devrait contenir: manage links, start/stop trace

	- Le menu "Macro" est à fusionner avec Tools parce que ça fait la même chose dans l'idéee

	- Le menu "Help", personne ne l'ouvre jamais, donc on peut l'enlever. Peut-être mettre un lien dans les settings.
----> issue faite

[Bug] lancer un script avec "show()" depuis la GUI themys renvoie une erreur
----> issue faite

[Remarque] il faudrait un bouton cochable pour passer en mode "rescale to data range" automatiquement
----> c'est deja le cas: dans le color map panel: "automatic rescale range mode" (passer de "update when apply" a "clamp and update every timestep"). Possibilite de le mettre par defaut dans les settings (et donc dans le .json)  [grow and update: maj quand les min/max sortent des range; clamp: maj a chaque timestep]

[Question] Est-ce qu'il y a un dark mode à paraview ?
----> a paraview non, il faut passer par un fichier de conf de QT, voir une install de Qtct
(tuto:https://discourse.paraview.org/t/how-to-get-paraview-dark-ui-any-os-any-release/4545)

[Question] Comment délier deux cameras ?
----> dans le menu "tools" -> "manage link" -> "delete"

[Question] Est-ce possible de faire une rotation ou symetrie facilement ? c-a-d avoir sur un meme layout deux fois une base: une a l'endroit et l'autre en inverse avec un simple bouton.
----> le filtre transform et reflect permettent de faire ça. mais c'est un peu compliqué -> issue faite

[Remarque] l'option "find data" permet de faire des queries mais seulement avec des et logiques. On voudait des ou aussi. et des intervales de valeurs, et des valeurs discontinues aussi. c-a-d pourvoir choisir toutes les mailles dont l'id = 2, 5, 20.
----> les intervalles de valeurs et les valeurs discontinues sont deja prises en compte. Les "Ou" aussi en faisant "query" "is" "a<100 | b >2"

[Remarque]  Il faudrait faire evoluer le threshold pour choisir plusieurs sous-domaines discontinu.
----> nan le find data permet de faire un extract selection
